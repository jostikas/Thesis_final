INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_PRBS9 prbs9)

FIND_PATH(
    PRBS9_INCLUDE_DIRS
    NAMES prbs9/api.h
    HINTS $ENV{PRBS9_DIR}/include
        ${PC_PRBS9_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    PRBS9_LIBRARIES
    NAMES gnuradio-prbs9
    HINTS $ENV{PRBS9_DIR}/lib
        ${PC_PRBS9_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(PRBS9 DEFAULT_MSG PRBS9_LIBRARIES PRBS9_INCLUDE_DIRS)
MARK_AS_ADVANCED(PRBS9_LIBRARIES PRBS9_INCLUDE_DIRS)

