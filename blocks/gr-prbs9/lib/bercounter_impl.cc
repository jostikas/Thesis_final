/* -*- c++ -*- */
/* 
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "bercounter_impl.h"
#include <cstdio>

#define VERBOSE 0
namespace gr {
  namespace prbs9 {

    bercounter::sptr
    bercounter::make(double update_time)
    {
      return gnuradio::get_initial_sptr
        (new bercounter_impl(update_time));
    }

    uint8_t
    bercounter_impl::next_bit()
    {
      uint8_t output_bit = d_reg&1;
  
      /* Update the state */
      int newbit = (((d_reg >> 8) ^ (d_reg >> 4)) & 0x0001); //x^9+x^5+1, PRBS9
      d_reg = ((d_reg << 1) | newbit) & 0x01FF;
      return output_bit;
    }

    uint16_t
    bercounter_impl::pack_16(uint16_t startindex, const uint8_t* d)
    {
      uint16_t ret;
      for (int i = startindex; i < startindex+16; i++)
      {
        ret = (ret<<1) | d[i];
      }
      return ret;
    }

    void
    bercounter_impl::process(pmt::pmt_t pdu)
    {
      uint16_t bits1 = 0;
      uint16_t bits2 = 0;
      uint8_t is_flipped = 0;

      /* check and extract the register state */
      pmt::pmt_t vector = pmt::cdr(pdu);
      size_t len = pmt::blob_length(vector);
      if (VERBOSE && len != 260*8)
      {
        std::cerr << "Bloblength was wrong: " << len << std::endl;
        return;
      }
      size_t offset(0);
      const uint8_t* d = (const uint8_t*) pmt::uniform_vector_elements(vector, offset);
      bits1 = pack_16(0, d);
      if ( !((bits1 ^ pack_16(16, d)) & 0x01FF) )
      {
        if (d_reg && (d_reg != bits1)) d_lost++;
        d_reg = bits1;
        //std::cout << "Reg states matched. " <<std::endl;
      }
      else
      {
        std::cout << "Reg states didn't match: " <<std::endl;
        std::cout << std::bitset<16>(bits1&0x01FF) <<std::endl;
        std::cout << std::bitset<16>(pack_16(16, d)&0x01FF) << std::endl;
        //std::cout << "R";
        return;
      }
      if (VERBOSE) std::cout << "Reg state: " << std::bitset<16>(d_reg);

      bits1=0;

      /* read bits one by one, push into registers. If registers are complementary, flip.
      */
      for (int i=32; i < len; i++)
      {
        uint8_t actual = next_bit();
        uint8_t recvd = d[i] ^ is_flipped;

        if ( actual != recvd) d_bit_errors++;
        bits1 = (bits1<<1) | actual;
        bits2 = (bits2<<1) | recvd;
        if ((bits1 ^ bits2) == 0xFFFF)
        {
          bits2 ^= 0xFFFF;
          
          d_bit_errors -= 16;
          is_flipped ^= 1;
          std::cout <<"F"<<is_flipped<<std::endl;
          d_flips += 1;
        }
      }
      d_total_bits += len-32;
      boost::posix_time::ptime now(boost::posix_time::microsec_clock::local_time());
        boost::posix_time::time_duration diff = now - d_last_update;
        double diff_ms = diff.total_milliseconds();
        if(diff_ms >= d_update_time)
        {
          d_last_update = now;
          std::cout << "*********BER**********" << std::endl;
          std::cout << "BTS: " << std::setprecision(4) << double(d_total_bits) <<std::endl;
          std::cout << "BER: " << double(d_bit_errors)/double(d_total_bits) << std::endl;
          std::cout << "SNR: " << d_last_snr << std::endl;
          std::cout << "BE : " << d_bit_errors << std::endl; 
          std::cout << "FL : " << d_flips << std::endl;
          std::cout << "LST: " << d_lost << std::endl;
          std::cout << "**********************" << std::endl;
        }
    }

    void
    bercounter_impl::snr(pmt::pmt_t snr)
    {
      d_last_snr = pmt::to_double(snr);
    }

    /*
     * The private constructor
     */
    bercounter_impl::bercounter_impl(double update_time)
      : gr::block("bercounter",
              gr::io_signature::make(0, 0, 0),
              gr::io_signature::make(0, 0, 0)),
        d_reg(0), d_update_time(update_time*1000), 
        d_last_update(boost::posix_time::microsec_clock::local_time()),
        d_total_bits(0), d_bit_errors(0), d_flips(0), d_lost(0), d_last_snr(0)
    {
      message_port_register_in(pmt::mp("pdus"));
      set_msg_handler(pmt::mp("pdus"), boost::bind(&bercounter_impl::process, this, _1));

      message_port_register_in(pmt::mp("snr"));
      set_msg_handler(pmt::mp("snr"), boost::bind(&bercounter_impl::snr, this, _1));
    }

    /*
     * Our virtual destructor.
     */
    bercounter_impl::~bercounter_impl()
    {
    }

  } /* namespace prbs9 */
} /* namespace gr */

