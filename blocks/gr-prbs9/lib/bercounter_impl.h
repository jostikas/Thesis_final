/* -*- c++ -*- */
/* 
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_PRBS9_BERCOUNTER_IMPL_H
#define INCLUDED_PRBS9_BERCOUNTER_IMPL_H

#include <prbs9/bercounter.h>
#include <pmt/pmt.h>
#include <iostream>
#include <sstream>
#include <gnuradio/blocks/file_sink_base.h>

namespace gr {
  namespace prbs9 {

    class bercounter_impl : public bercounter
    {
     private:
      uint16_t d_reg;
      double d_update_time;
      boost::posix_time::ptime d_last_update;
      uint32_t d_total_bits;
      uint32_t d_bit_errors;
      uint32_t d_flips;
      uint32_t d_lost;
      double d_last_snr;

      /* Return next bit of PRBS9. */
      uint8_t next_bit(void);

      /* Pack 16 bits from pdu into uint16_t, starting from startindex */
      uint16_t pack_16(uint16_t startindex, const uint8_t* d);

      /* Process incoming pdus-s */
      void process(pmt::pmt_t pdu);

      /* Store snr value */
      void snr(pmt::pmt_t snr);


     public:
      bercounter_impl(double update_time);
      ~bercounter_impl();
    };

  } // namespace prbs9
} // namespace gr

#endif /* INCLUDED_PRBS9_BERCOUNTER_IMPL_H */

