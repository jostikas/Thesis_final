/* -*- c++ -*- */

#define PRBS9_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "prbs9_swig_doc.i"

%{
#include "prbs9/bercounter.h"
%}


%include "prbs9/bercounter.h"
GR_SWIG_BLOCK_MAGIC2(prbs9, bercounter);
