#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2016 <+YOU OR YOUR COMPANY+>.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks, analog
import pmt
import doppler_swig as doppler
import numpy as np


def make_tag(key, value, offset, srcid=None):
    tag = gr.tag_t()
    tag.key = pmt.string_to_symbol(key)
    tag.value = pmt.to_pmt(value)
    tag.offset = offset
    if srcid is not None:
        tag.srcid = pmt.to_pmt(srcid)
    return tag


class qa_doppler_tle_cc (gr_unittest.TestCase):
    params = dict(
        samp_rate=32000,
        center_freq=1e9,
        obs=(58.39097, 26.62874, 70.0),
        # lat=58.39097,
        # lon=26.62874,
        # alt=70,
        use_tags=True,
        tle="""COMPASS-1               
1 32787U 08021E   15034.18844670  .00005758  00000-0  60580-3 0  9996
2 32787  97.6700  85.9343 0014488 134.7022 225.5379 14.89702527366557
"""
    )

    def setUp(self):
        self.tb = gr.top_block()

    def tearDown(self):
        self.tb = None

    def test_001_t(self):
        # Test the working of the CC TLE predictor

        # Load unix times and corresponding doppler shifts.
        # Files are 20 seconds' unix time stamps and doppler shifts as doubles
        # for parameters as described above.
        samp_rate = self.params['samp_rate']
        uts = np.fromfile(
            open('qa_data/qa_doppler_tle_uts.f64'), dtype=np.float64)
        dops = np.fromfile(
            open('qa_data/qa_doppler_tle_dops.f64'), dtype=np.float64)

        # Generate the source data.
        src_data = np.ones(19 * samp_rate + 1, dtype=np.complex64).tolist()
        print "len: ", len(src_data)
        # Generate expected data.
        exp_data = np.empty(len(src_data))
        for index in range(len(dops) - 1):
            start = index * samp_rate
            stop = (index + 1) * samp_rate
            exp_data[start:stop] = np.linspace(dops[index],
                                               dops[index + 1],
                                               samp_rate,
                                               endpoint=False,
                                               )
            exp_data[-1] = dops[-1]  # Fill in the last data point.
        

        # Create the inital "rx_time" tag
        secs = int(uts[0])
        fracs = uts[0] - secs
        src_tags = tuple([make_tag('rx_time', 
                                   (secs, fracs), 
                                   0,
                                   #int(3.2*samp_rate), 
                                   'src'
                                   )
        ])

        # Construct flowgraph
        src = blocks.vector_source_c(src_data, repeat=False, tags=src_tags)
        dst_src = blocks.file_meta_sink(gr.sizeof_gr_complex*1, "/home/ec/Desktop/src_data.cf32", samp_rate, 1, blocks.GR_FILE_FLOAT, True, 1000000, "", True)
        blk = doppler.doppler_tle_cc(**self.params)
        dif = analog.quadrature_demod_cf(samp_rate / np.pi / 2)
        dst = blocks.vector_sink_f()
        dst_cmp = blocks.vector_sink_c()
        self.tb.connect(src, blk, dif, dst)
        self.tb.connect(blk, dst_cmp)
        self.tb.connect(src, dst_src)
        # Run and gather data
        self.tb.run()
        out_data = dst.data()
        #exp_data.tofile("/home/ec/Desktop/exd.f32")
        #np_out = np.array(out_data, dtype=float)
        #np_out.tofile("/home/ec/Desktop/out_float.f32")
        #cmp_data = dst_cmp.data()
        #np_out = np.array(cmp_data, dtype=np.complex64)
        #np_out.tofile("/home/ec/Desktop/out_cmp.cf32")

        self.assertFloatTuplesAlmostEqual2(out_data[1:], exp_data[1:].tolist(), rel_eps=5e-5)

#    def test_002_t(self):
#        # Test the working of the CC TLE predictor
#        self.skipTest("Not implemented.")
#        src_data = tuple([1, 2.3j, 4, 5.6, 7j])
#        exp_data = src_data
#
#        # Create the inital "rx_time" tag
#        secs = int(1011.33531)
#        fracs = 1011.33531 - secs
#        src_tags = tuple([make_tag('rx_time', (secs, fracs), 0, 'src')])
#
#        src = blocks.vector_source_c(src_data, repeat=False, tags=src_tags)
#        blk = doppler.doppler_tle_cc(**self.params)
#        dst = blocks.vector_sink_c()
#
#        self.tb.connect(src, blk, dst)
#        # Run and gather data
#        self.tb.run()
#
#        try:
#            self.assertComplexTuplesAlmostEqual(dst.data()[1:], exp_data, places=6)
#        except AssertionError:
#            print "Failed dummy block test."
#            raise

    def test_003_t(self):
        # Test the getter functions.
        # Setters are used when setting up block.
        def doTest(ret, test):
            self.assertEqual(ret, self.params[test], '\n'.join(
                ('Failed {} getter test'.format(test),
                 'Expected: "{}"'.format(self.params[test]),
                 'Got: "{}"'.format(ret)
                 )
            )
            )
        # The constructor uses all setters, so no need to test explicitly.
        blk = doppler.doppler_tle_cc(**self.params)

        ret = blk.tle()
        doTest(ret, 'tle')

        ret = blk.samp_rate()
        doTest(ret, 'samp_rate')

        ret = blk.obs()
        self.assertFloatTuplesAlmostEqual(ret, self.params['obs'], 7, '\n'.join(
            ('Failed {} getter test'.format('obs'),
             'Expected: "{}"'.format(self.params['obs']),
             'Got: "{}"'.format(ret)
             )
        )
        )
        ret = blk.use_tags()
        doTest(ret, 'use_tags')

        ret = blk.center_freq()
        doTest(ret, 'center_freq')


if __name__ == '__main__':
    gr_unittest.run(qa_doppler_tle_cc, "qa_doppler_tle_cc.xml")
