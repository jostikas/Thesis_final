/* -*- c++ -*- */
/* 
 * Copyright 2016 Laur Joost.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <cstdio>
#include <ctime>
#include <stdexcept>
#include <gnuradio/io_signature.h>
#include <gnuradio/tags.h>
#include <gnuradio/expj.h>
#include "doppler_tle_cc_impl.h"

#include <cassert>

#define MILLION 1000000
#define BILLION 1000000000 
#define DOP_PERIOD_IN_SECS 1  /*!< Period of updating the frequency prediction. */
#define DOP_PERIOD d_samp_rate*DOP_PERIOD_IN_SECS  /*!< Period of updating the frequency in samples */
#define LC_KM 299792.458 /*<! Speed of light in km/s */

namespace gr {
  namespace doppler {

    doppler_tle_cc::sptr
    doppler_tle_cc::make(
        double samp_rate,
        double center_freq,
        char tle[240],
        std::vector<double> obs,
        bool use_tags)
    {
      return gnuradio::get_initial_sptr
        (new doppler_tle_cc_impl(samp_rate, center_freq, tle, obs, use_tags));
    }

    /*
     * The private constructor
     */
    doppler_tle_cc_impl::doppler_tle_cc_impl(
        double samp_rate,
        double center_freq,
        char tle_string[240],
        std::vector<double> obs,
        bool use_tags)
      : gr::sync_block("doppler_tle_cc",
              gr::io_signature::make(1, 1, sizeof(gr_complex)),
              gr::io_signature::make(1, 1, sizeof(gr_complex)))
    {
      /* Allocate memory for structs */
      d_sat = new sat_t();
      d_obs = new geodetic_t();

      /* Initialize other member variables */
      d_renew = false;
      d_rx_time = uhd::time_spec_t();
      d_offset = uhd::time_spec_t();
      d_systime_initialized = false;
      d_samps_to_next = 0;
      d_nco = gr_expj(0);
      d_end_freq = 0;
      d_curr_tag = tag_t();      

      set_samp_rate(samp_rate);
      set_center_freq(center_freq);
      set_tle(tle_string);
      set_obs(obs);
      set_use_tags(use_tags);
    }

    /*
     * Our virtual destructor.
     */
    doppler_tle_cc_impl::~doppler_tle_cc_impl()
    {
      delete d_sat;
      delete d_obs;
    }

    /*************************************/
    /*      Getters and setters          */
    /*************************************/

    /* Sample rate */
    void
    doppler_tle_cc_impl::set_samp_rate(double samp_rate)
    {
      d_samp_rate = samp_rate;
      d_freqs.reserve(int(DOP_PERIOD));
      d_renew = true;
    }

    double
    doppler_tle_cc_impl::samp_rate(void)
    {
      return d_samp_rate;
    }

    /* Center frequency */
    void
    doppler_tle_cc_impl::set_center_freq(double center_freq)
    {
      d_center_freq = center_freq;
      d_renew = true;
    }

    double
    doppler_tle_cc_impl::center_freq(void)
    {
      return d_center_freq;
    }

    /* TLE - ie. satellite selection */
    void
    doppler_tle_cc_impl::set_tle(char tle_string[240])
    {
      char tle_lines[3][80];

      /* Split the TLE into lines */
      std::stringstream ss(tle_string);
      std::string to;
      for (int i=0;i<3;i++)
      {
        std::getline(ss, to);
        strncpy(tle_lines[i], to.c_str(), 80);
        tle_lines[i][79] = '\0';
      }

      /* Process the TLE */
      if(Get_Next_Tle_Set(tle_lines, &d_sat->tle) != 1)
      {
        throw std::invalid_argument("TLE string failed checksum!");
      } 
      else
      {
        select_ephemeris(d_sat);
        memcpy(d_tle_string, tle_string, 240);  /* Lookup buffer for returning only. */
        d_renew = true;
        if (d_debug) std::cout << "TLE: " << tle() << std::endl;
      }
    }

    char *
    doppler_tle_cc_impl::tle(void)
    {
      return d_tle_string;
    }

    void
    doppler_tle_cc_impl::set_obs(std::vector<double> obs)
    {
      d_obs->lat = Radians(obs[0]);
      d_obs->lon = Radians(obs[1]);
      d_obs->alt = obs[2]/1000;  /* Model wants this in km-s */
      d_obs->theta = 0;
      d_renew = true;
      if (d_debug) std::cout << "OBS: (" << this->obs()[0] << ", " << this->obs()[1] << ", " << this->obs()[2] << ")" << std::endl;
    }

    std::vector<double>
    doppler_tle_cc_impl::obs(void)
    {
      double ret[] = {Degrees(d_obs->lat), Degrees(d_obs->lon), d_obs->alt*1000};
      return std::vector<double>(ret, ret+3);
    }

    void
    doppler_tle_cc_impl::set_use_tags(bool use_tags)
    {
      d_use_tags = use_tags;
    }

    bool
    doppler_tle_cc_impl::use_tags(void)
    {
      return d_use_tags;
    }

    uhd::time_spec_t
    doppler_tle_cc_impl::tag_to_time_spec(tag_t tag)
    {
      if (d_debug) {
        std::cout << "TTTS: "<< tag.key << tag.value << std::endl;
        assert (pmt::symbol_to_string(tag.key) == "rx_time");
      }
      
      time_t full_secs = (time_t) pmt::to_uint64(pmt::tuple_ref(tag.value, 0));
      double frac_secs = pmt::to_double(pmt::tuple_ref(tag.value, 1));
      assert(0 <= frac_secs && frac_secs < 1);

      return uhd::time_spec_t(full_secs, frac_secs);
    }

    inline double
    doppler_tle_cc_impl::unix_to_jd(uhd::time_spec_t rx_time)
    {
      if (d_debug) std::cout << "UTJD: "<< rx_time.get_real_secs() << std::endl;
      return double(rx_time.get_full_secs())/86400 + rx_time.get_frac_secs()/86400 + 2440587.5;
    }

    /* Does the actual calling of the library. Copied wholesale from the original C program.
      Note that this section is only slightly modified for the thesis from earlier work by Viljo Allik.*/
    void
    doppler_tle_cc_impl::predict(uhd::time_spec_t rx_time)
    {
      obs_set_t     obs_set;
      geodetic_t    sat_geodetic;
      double        age;
      double jdtime = unix_to_jd(rx_time);
      if (d_debug)  std::cout << "PRED: JD"<< jdtime << std::endl;
      
      d_sat->jul_epoch = Julian_Date_of_Epoch (d_sat->tle.epoch); // => tsince = 0.0
      d_sat->jul_utc = jdtime;
      d_sat->tsince = (d_sat->jul_utc - d_sat->jul_epoch) * xmnpda;
    //    d_sat->tsince = (d_sat->jul_utc - d_sat->jul_epoch);

      /* call the norad routines according to the deep-space flag */
     if (d_sat->flags & DEEP_SPACE_EPHEM_FLAG)
          SDP4 (d_sat, d_sat->tsince);
      else 
          SGP4 (d_sat, d_sat->tsince);

      /* Convert to km-s */
      Convert_Sat_State (&d_sat->pos, &d_sat->vel);

      /* get the velocity of the satellite */
      Magnitude (&d_sat->vel);
      d_sat->velo = d_sat->vel.w;
      Calculate_Obs (d_sat->jul_utc, &d_sat->pos, &d_sat->vel, d_obs, &obs_set);
      Calculate_LatLonAlt (d_sat->jul_utc, &d_sat->pos, &sat_geodetic);

      while (sat_geodetic.lon < -pi)
          sat_geodetic.lon += twopi;

      while (sat_geodetic.lon > (pi))
          sat_geodetic.lon -= twopi;

      d_sat->az = Degrees (obs_set.az);
      d_sat->el = Degrees (obs_set.el);
      d_sat->range = obs_set.range;
      d_sat->range_rate = obs_set.range_rate;
      if (d_debug)  std::cout << "PRED: RR "<< d_sat->range_rate << std::endl;
      d_sat->ssplat = Degrees (sat_geodetic.lat);
      d_sat->ssplon = Degrees (sat_geodetic.lon);
      d_sat->alt = sat_geodetic.alt;
      d_sat->ma = Degrees (d_sat->phase);
      d_sat->ma *= 256.0/360.0;
      d_sat->phase = Degrees (d_sat->phase);

      /* same formulas, but the one from predict is nicer */
      //d_sat->footprint = 2.0 * xkmper * acos (xkmper/d_sat->pos.w);
      d_sat->footprint = 12756.33 * acos (xkmper / (xkmper+d_sat->alt));
      age = d_sat->jul_utc - d_sat->jul_epoch;
      d_sat->orbit = (long) floor((d_sat->tle.xno * xmnpda/twopi +
                      age * d_sat->tle.bstar * ae) * age +
                      d_sat->tle.xmo/twopi) + d_sat->tle.revnum - 1;
    }

    void
    doppler_tle_cc_impl::linterp(std::vector<float> &freq,

        double start_freq,
        double end_freq,
        int samps_to_next
        )
    {
      freq.clear();
      if (d_debug) {
        std::cout << "LINT: SF "<< start_freq << std::endl;
        std::cout << "LINT: EF "<< end_freq << std::endl;
        std::cout << "LINT: STN "<< samps_to_next << std::endl;  
      }

      for (int i=0;i < samps_to_next;i++)
      {
        freq.push_back(float(start_freq + i * (end_freq-start_freq) / samps_to_next));
      }
    }

    uhd::time_spec_t
    doppler_tle_cc_impl::get_system_time()
    {
      using namespace uhd;
      time_spec_t ts;
      struct timeval tmval;
      

      if ( d_systime_initialized )
      {
        return time_spec_t::get_system_time() + d_offset;
      }
      else
      {
        d_systime_initialized = true;
        ts = time_spec_t::get_system_time();
        gettimeofday(&tmval, NULL);
        d_offset = time_spec_t::get_system_time() + ts; 
        d_offset = time_spec_t(tmval.tv_sec, tmval.tv_usec, MILLION) - 
          time_spec_t(d_offset.get_real_secs() / 2);

        if (d_debug) std::cout << "SYST: F "<< (ts + d_offset).get_real_secs() << std::endl;
        return ts + d_offset;
      }
    }

    int
    doppler_tle_cc_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const gr_complex *in = (const gr_complex *) input_items[0];
      gr_complex *out = (gr_complex *) output_items[0];
 
      std::cout.precision(15);

      using std::deque;          

      /********************************/
      /*       Predictor logic        */
      /********************************/

      bool renew = false;
      uhd::time_spec_t time_next = uhd::time_spec_t();

      if ( !d_samps_to_next ) 
      {  // If we're out of predictions
        if (d_debug)  std::cout << "OOP" << std::endl;
        renew = true;
        d_samps_to_next = DOP_PERIOD;  // This will later be reduced, if necessary.
      }

      /* First case:
       * Using accurate reception time stamps from the radio.
       *
       * If there are any tags not on the first sample, we'll only process up
       * until them, and let the next work() call deal with the time change.
       */     
      if (d_use_tags)
      {
        std::vector<tag_t> tags;
        get_tags_in_window(tags, 0, 0, noutput_items, pmt::intern("rx_time"));
        deque<tag_t> tags_d(tags.begin(), tags.end());
        tags.clear();
        while ( !tags_d.empty() ) 
        {
          if ( tags_d.front().offset == nitems_read(0) ) {
            if (d_debug) {
              std::cout << "TAG@FRONT: "<< tags_d[0].key << std::endl;
              std::cout << "      VAL: "<< tags_d[0].value << std::endl;
              std::cout << "   OFFSET: "<< tags_d[0].offset << std::endl;
            }
            
            d_curr_tag = tags_d.front();
            tags_d.pop_front();
            d_samps_to_next = DOP_PERIOD;
            renew = true;
            d_end_freq = 0;  //Signals that we can't use cached frequency value.
          }
          else {  
            /* Process no more than up to the next tag or to current next time, if earlier. */
            if (d_debug) {
              std::cout << "TAG@LATER: "<< tags_d[0].key << std::endl;
              std::cout << "      VAL: "<< tags_d[0].value << std::endl;
              std::cout << "   OFFSET: "<< tags_d[0].offset << std::endl;
              std::cout << "RELOFFSET: "<< tags_d[0].offset-nitems_read(0) << std::endl;
            }
            
            uint rel_offset = tags_d[0].offset - nitems_read(0);
            if (rel_offset < d_samps_to_next) {
              d_samps_to_next = rel_offset;
              if (d_debug) {
                std::cout << " STN: "<< d_samps_to_next << std::endl;
                std::cout << "SIZEFREQS: "<< d_freqs.size() << std::endl;
              } 
            }
            break;
          }
        }
        if (renew ) // Calculate timestamps for renewal
        {
          /* Calculate current time from last received tag and number of samples processed.
           * That way we avoid error accumulation
           */
          if (d_curr_tag.key != pmt::intern("rx_time"))
          {
            std::cout << "Doppler block says: " << std::endl;
            std::cout << "No starting rx_time tag found. Using current time as starting point.\n"
                         "(If your hardware driver doesn't generate such tags, use 'use_tags=False',\n"
                         "as tagged mode only updates based on tags and sample rate)." << std::endl;
            uhd::time_spec_t curr_time = get_system_time();
            d_curr_tag.key = pmt::intern("rx_time");
            d_curr_tag.value = pmt::make_tuple(
              pmt::from_uint64(curr_time.get_full_secs()),
              pmt::from_double(curr_time.get_frac_secs())
            );
            d_curr_tag.offset = nitems_read(0);
          }
          d_rx_time = tag_to_time_spec(d_curr_tag) + 
            uhd::time_spec_t(double(nitems_read(0) - d_curr_tag.offset) / d_samp_rate);
          time_next = d_rx_time + uhd::time_spec_t(double(d_samps_to_next)/d_samp_rate);
          if (d_debug) {
            std::cout << "RENEW_wTAGS" << std::endl;
            std::cout << "      RXT: " << d_rx_time.get_real_secs() << std::endl;
            std::cout << "      NXT: " << time_next.get_real_secs() << std::endl;
          }
        }
      }

      /*! Second case:
       * Using wall clock time at the moment that work() is called.
       * Will use a higher update rate, to recover quicker from dropped packets.
       */
      else
      {
        uhd::time_spec_t curr_time = get_system_time();
        if ((curr_time - d_rx_time).get_real_secs() > DOP_PERIOD/2)
        {
          renew = true;
          d_end_freq = 0;  // Also need to calculate new start frequency.
          std::cout << "J";
        }

        if (renew)
        {
          d_rx_time = get_system_time();
          d_samps_to_next = DOP_PERIOD/2;  //Update prediction
          time_next = d_rx_time + uhd::time_spec_t(double(d_samps_to_next)/d_samp_rate);
          if (d_debug) {
            std::cout << "RENEW_wTIME" << std::endl;
          }
        }
      }

      /* If necessary, generate a new prediction vector. */
      if (renew)
      {
        double start_freq;

        /* Get the timestamps to use for prediction. */
        if ( d_end_freq ) {  // Use cached value, if possible
          start_freq = d_end_freq;
        }
        else {
          predict(d_rx_time);
          if (d_debug) {
            std::cout << "RAW_SFREQ" << (d_sat->range_rate/LC_KM) * d_center_freq *(-1.0) << std::endl;
          }
          start_freq = (d_sat->range_rate/LC_KM) * d_center_freq *(-1.0) * twopi / d_samp_rate;
        }
        predict(time_next);
        if (d_debug) {
            std::cout << "RAW_EFREQ" << (d_sat->range_rate/LC_KM) * d_center_freq *(-1.0) << std::endl;
          }
        d_end_freq = (d_sat->range_rate/LC_KM) * d_center_freq *(-1.0) * twopi / d_samp_rate;

        /* Update the d_freqs vector */
        linterp(d_freqs, start_freq, d_end_freq, d_samps_to_next);
        d_freqs_idx = 0;
      }

      /* Do actual work. */
      if ( d_samps_to_next < noutput_items) {
        noutput_items = d_samps_to_next;
      }

      for (int i = 0; i<noutput_items; i++)
      {
        d_nco *= gr_expj(d_freqs[d_freqs_idx+i]);
        out[i] = d_nco * in[i];
      }
      d_samps_to_next -= noutput_items;
      d_freqs_idx += noutput_items;

      //memcpy(out, in, noutput_items * sizeof (in[0]));
      return noutput_items;
    }
  } /* namespace doppler */
} /* namespace gr */
