# Build library as object files for linking with doppler_tle_cc
set(sgpsdp_sources
	sgp4sdp4.c
	sgp_in.c
	sgp_math.c
	sgp_obs.c
	sgp_time.c
	#solar.c  # Don't need that
	)

add_library(sgpsdp OBJECT ${sgpsdp_sources})
target_compile_options(sgpsdp PRIVATE -fPIC -Wall -Wextra)