#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include "sgpsdp/sgp4sdp4.h"
#include <sys/time.h>
#define TXFR 145.980
#define F_SCALE (( -145.98 / 100. ) / 0.0117)
FILE *pf;
sat_t Sat;
double d100m;
geodetic_t my_location;
double Longitude = 26.62874;
double Latitude = 58.39097;
double Altitude = 70; 
int  tfd=0;
double DopplerInterval=1.0/86400; // interval in JDays between 2 corrections (1s)

int LoadTLE( char *tle_pathname, char* satname, sat_t *sat ) 
{
	FILE *fp;
	char tle_str[3][80];
	char str[128];
	char *b;
	fp = fopen (tle_pathname, "r");
	if (fp == NULL)
	{
		sprintf( str," Cannot open tle file %s\n", tle_pathname );
		fputs( str, stderr );		
		return -1;
	}
	while(1)
	{
		b = fgets(tle_str[0], 80, fp);
		if( b == NULL )
		{
			sprintf( str," Cannot find satellite %s in TLE file %s\n", satname, tle_pathname );
			fputs( str, stderr );		
	        fclose (fp);
			return -1;
		}
		else if( strncmp( b, satname, strlen(satname)) == 0 ) break;
	}
	b = fgets(tle_str[1], 80, fp);
	b = fgets(tle_str[2], 80, fp);
    fclose (fp);
	if( b == NULL )
	{
		sprintf( str," Cannot find satellite %s TLE data in file %s, unexpected EOF!\n", satname, tle_pathname );
		fputs( str, stderr );		
	
		return -1;
	}

    if (Get_Next_Tle_Set (tle_str, &(sat->tle)) != 1)
	{
        sprintf(str, "Invalid TLE data! \n");
	fputs( str, stderr );		
		
        return -1;
    }
	my_location.lat=Radians(Latitude);
	my_location.lon=Radians(Longitude);
	my_location.alt=Altitude/1000.; // km!
	my_location.theta=0.;
	select_ephemeris(sat);
	return 1;
}

void PredictCalc(sat_t *sat, geodetic_t *obs_geodetic, double t) 
{
    obs_set_t     obs_set;
    geodetic_t    sat_geodetic;
    double        age;
    double jul_utc;
    
    sat->jul_epoch = Julian_Date_of_Epoch (sat->tle.epoch); // => tsince = 0.0
    sat->jul_utc = t;
    sat->tsince = (sat->jul_utc - sat->jul_epoch) * xmnpda;
	//    sat->tsince = (sat->jul_utc - sat->jul_epoch);

    /* call the norad routines according to the deep-space flag */
   if (sat->flags & DEEP_SPACE_EPHEM_FLAG)
        SDP4 (sat, sat->tsince);
    else 
        SGP4 (sat, sat->tsince);

    /* Convert to km-s */
    Convert_Sat_State (&sat->pos, &sat->vel);

    /* get the velocity of the satellite */
    Magnitude (&sat->vel);
    sat->velo = sat->vel.w;
    Calculate_Obs (sat->jul_utc, &sat->pos, &sat->vel, obs_geodetic, &obs_set);
    Calculate_LatLonAlt (sat->jul_utc, &sat->pos, &sat_geodetic);

    while (sat_geodetic.lon < -pi)
        sat_geodetic.lon += twopi;

    while (sat_geodetic.lon > (pi))
        sat_geodetic.lon -= twopi;

    sat->az = Degrees (obs_set.az);
    sat->el = Degrees (obs_set.el);
    sat->range = obs_set.range;
    sat->range_rate = obs_set.range_rate;
    sat->ssplat = Degrees (sat_geodetic.lat);
    sat->ssplon = Degrees (sat_geodetic.lon);
    sat->alt = sat_geodetic.alt;
    sat->ma = Degrees (sat->phase);
    sat->ma *= 256.0/360.0;
    sat->phase = Degrees (sat->phase);

    /* same formulas, but the one from predict is nicer */
    //sat->footprint = 2.0 * xkmper * acos (xkmper/sat->pos.w);
    sat->footprint = 12756.33 * acos (xkmper / (xkmper+sat->alt));
    age = sat->jul_utc - sat->jul_epoch;
    sat->orbit = (long) floor((sat->tle.xno * xmnpda/twopi +
                    age * sat->tle.bstar * ae) * age +
                    sat->tle.xmo/twopi) + sat->tle.revnum - 1;
}

/** \brief Get the current time.
 *
 * Read the system clock and return the current Julian day.
 */
double get_current_daynum ()
{
    struct tm utc;
    struct timeval tmval;
    double daynum;

    UTC_Calendar_Now (&utc);
    gettimeofday (&tmval, NULL);
//    g_get_current_time (&tmval);
    daynum = Julian_Date (&utc);
    daynum = daynum + (double)tmval.tv_usec/8.64e+10;
    return daynum;
}

//#define gen 1  /* When using the file for generating a testcase for gr-doppler */
//#if !gen
//main( int ac, char *av[] )
//{
//    char line[128];
//    char date[16];
//    char tim[16];
//    char otxt[128];
//    float az;
//    float el;
//    int range;
//    int footp;
//    int delta_t;
//    float vel;
//    double dop, dop0, dop1;
//    double f_nom;
//    double UT;
//    float loss;
//    struct tm tc;
//    double doppler; 
//    int i, n, skip;
//    struct termios tset;
//    time_t t;
//    long fofs;
//    if( ac < 5 ) 
//    {
//	fprintf(stderr, "usage: %s tle_file_name sat_name nominal_freq time_interval_s\n", av[0]);
//	exit(0); 
//    }
//    f_nom=atof(av[3]);
//    delta_t=atoi(av[4]);
//    LoadTLE( av[1], av[2], &Sat );
///*
//    while(1)
//    {
//      PredictCalc(&Sat, &my_location, get_current_daynum()); 
//      fprintf( stdout, "%s:JDay=%12.5lf, Az=%6.1lf, El=%6.1lf, Range rate=%6.3lf", av[2],Sat.jul_utc, Sat.az, Sat.el, Sat.range_rate );
//      d100m = (Sat.range_rate/300000.0) * f_nom *(-1.0);
//      fprintf( stdout,"Doppler=%6.1lf\n", d100m); 
//      sleep(5);
//    }
//*/    
//    while(1)
//     {
//	PredictCalc(&Sat, &my_location, get_current_daynum());
//	if( Sat.el < (-1) )
//	{
//	     sleep(5);
//	     continue;	
//	} 
// 	dop0 = (Sat.range_rate/299792.458) * f_nom *(-1.0);
//	PredictCalc(&Sat, &my_location, get_current_daynum()+DopplerInterval); 
// 	dop1 = (Sat.range_rate/299792.458) * f_nom *(-1.0);
//	fofs=(long)((dop0 + dop1)/2);
//    UT = (Sat.jul_utc - 2440587.5) * 86400;
//	fprintf( stderr, "UT=%12.10lf, Doppler=%1.5f\n", UT, dop0 );
//	sleep(1);
//     }
//}
//#endif /* !gen */
//#if gen
//
//int main(int ac, char *av[])
//{
//    double UT[20];
//    double dops[20];
//    double f_nom=1e9;
//    FILE *ut_fp;
//    FILE *dops_fp;
//
//    LoadTLE( av[1], av[2], &Sat );
//
//    for(int idx=0;idx<20;idx++)
//    {
//        UT[idx] = 1461854821 + idx;
//        PredictCalc(&Sat, &my_location, UT[idx]/86400.0 + 2440587.5);
//        dops[idx] = (Sat.range_rate/299792.458) * f_nom *(-1.0);
//        printf("UT: %f, dop: %f\n", UT[idx], dops[idx]);
//    }
//    ut_fp = fopen("test_uts.f64","wb");
//    dops_fp = fopen("test_dops.f64","wb");
//    fwrite((void *)UT, sizeof UT[0], (sizeof UT)/sizeof UT[0], ut_fp);
//    fwrite((void *)dops, sizeof dops[0], (sizeof dops)/sizeof dops[0], dops_fp);
//    fclose(ut_fp);
//    fclose(dops_fp);
//    return EXIT_SUCCESS;  
//}
//#endif /* gen */