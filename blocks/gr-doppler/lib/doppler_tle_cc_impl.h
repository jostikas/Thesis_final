/* -*- c++ -*- */
/* 
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_DOPPLER_DOPPLER_TLE_CC_IMPL_H
#define INCLUDED_DOPPLER_DOPPLER_TLE_CC_IMPL_H

#include <doppler/doppler_tle_cc.h>
#include <uhd/types/time_spec.hpp>
extern "C" {
#include <sgp4sdp4.h>
}

namespace gr {
  namespace doppler {

    class doppler_tle_cc_impl : public doppler_tle_cc
    {
     private:
      /* if the optimizer is doing it's thing, this should get removed along with all the cout nonsense */
      static const bool d_debug = false;
      /************************************/
      /*   VARS EXPOSED THROUGH GET-SET   */
      /************************************/
      double d_samp_rate;  /*!< Sample rate through the block. */
      double d_center_freq;  /*!< Center frequency to calculate doppler shift for. */
      char d_tle_string[240]; /*!< Cache variable for the TLE string */
      sat_t *d_sat;  /*!< Stores the satellite data. */
      geodetic_t *d_obs;  /*!< Stores observer location */
      bool d_use_tags;  /*!< Whether we want to read rx_time tags or use wall time */
      
      /************************************/
      /*          INTERNAL VARS           */
      /************************************/
      /*! Used to mark that the frequency list needs renewal in the work function. */
      bool d_renew;
      /*! Time of the next received input sample batch, assuming no drops.
       * Alternatively, when use_tags==false, wall time at the start of work() call. */
      uhd::time_spec_t d_rx_time;
      /*! Offset between system clock and the used high-resolution clock */
      uhd::time_spec_t d_offset;
      /*! Used to determine if d_offset is valid */
      bool d_systime_initialized;
      /*! Samples to process until next doppler calculation */
      int64_t d_samps_to_next;
      /*! Instantaneous normalized frequencies for use by the numerically controlled oscillator, */
      std::vector<float> d_freqs;
      /*! Numerically controlled oscillator state */
      gr_complex d_nco;
      /*! Cached end frequency value so we don't have to recalculate. If 0, means we do have to recalc. */
      double d_end_freq;
      /*! Used in tagged mode, to track time from last known timestamp. */
      tag_t d_curr_tag;
      /*! Marks the position in the frequencies index */
      uint64_t d_freqs_idx;

      /*!
       * Return the rx_time from an rx_time tag
       */
      uhd::time_spec_t
      tag_to_time_spec(tag_t tag);

      /*!
       * Return the Julian Day value corresponding to input time
       */
      double
      unix_to_jd(uhd::time_spec_t rx_time);

      /*!
       * Run the SGP4/SDP4 model to update the satellite state for a specific time.
       */
      void
      predict(uhd::time_spec_t rx_time);

      /*! 
       * Linearly interpolate between start_freq and end_freq noninclusively,
       * for samps_to_next samples, store into freq.
       */
      void
      linterp(std::vector<float> &freq,
        double start_freq,
        double end_freq,
        int samps_to_next
        );

      /*!
       * \brief Fetch current system time, return as time_spec_t.
       *
       * Works around the issue with uhd::time_spec_t::get_system_time() that it uses
       * the highest resolution clock available, which may not have a known epoch.
       * Tries to evaluate the offset and correct the result.
       */
      uhd::time_spec_t
      get_system_time(void);

     public:
      doppler_tle_cc_impl(
          double samp_rate,
          double center_freq,
          char tle_string[240],
          std::vector<double> obs,
          bool use_tags);
      ~doppler_tle_cc_impl();

      void set_samp_rate(double samp_rate);
      double samp_rate(void);

      void set_center_freq(double center_freq);
      double center_freq(void);

      void set_tle(char tle[240]);
      char *tle(void);

      void set_obs(std::vector<double> obs);
      std::vector<double> obs(void);

      void set_use_tags(bool use_tags);
      bool use_tags(void);

      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);
    };
  } // namespace doppler
} // namespace gr

#endif /* INCLUDED_DOPPLER_DOPPLER_TLE_CC_IMPL_H */

