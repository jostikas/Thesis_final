/* -*- c++ -*- */
/* 
 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_DOPPLER_DOPPLER_TLE_CC_H
#define INCLUDED_DOPPLER_DOPPLER_TLE_CC_H

#include <doppler/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
  namespace doppler {

    /*!
     * \brief Correct doppler shift based on the input TLE.
     * \ingroup doppler
     *
     * \param samp_rate Sample rate
     * \param center_freq Original center frequency
     * \param tle_string NASA Two-Line Elements string (three lines, with the name line)
     * \param obs Ground station location, decimal degrees (lat, lon, alt). Altitude in meters.
     * \param use_tags Use the "rx_time" tag to set the reference time. Otherwise will use current time.
     * \note use_tags assumes that the device timestamp received is from the same epoch
     * as current system timestamp.
     * 
     * Will apply a continuous frequency translation based on the transmitted center frequency
     * and satellite ephemeris from TLE to the input stream. Ephemeris is calculated based on
     * the "rx_time" tag, if \p use_tags=True, else will use current wall time.
     * 
     * \note The block doesn't currently react to samp_rate or freq tags from the USRP. If implemented,
     * it would enable sample-synchronous frequency and sampling rate changes, but would demand that
     * the block be placed almost exactly after the USRP block (or at least, avoiding any rate or 
     * frequency changes between them). While I currently see no reason to not place it so, I also
     * currently foresee no need for such changes that couldn't more easily be avoided by other means.
     *
     * Will print "J" to stdout, if discontinuous time between calls to work in time based mode
     * has been detected.
     * 
     * Frequency correction is calculated once every second in tagged mode, once per half a second in
     * wall-time based mode, and linearly interpolated over the period.
     *
     * Uses WGS '72 datum throughout, as that is what SGP4 uses.
     */
    class DOPPLER_API doppler_tle_cc : virtual public gr::sync_block
    {
     public:
      typedef boost::shared_ptr<doppler_tle_cc> sptr;

      /* Not Doxygenated. Why should it be?
       * \brief Return a shared_ptr to a new instance of doppler::doppler_tle_cc.
       *
       * To avoid accidental use of raw pointers, doppler::doppler_tle_cc's
       * constructor is in a private implementation
       * class. doppler::doppler_tle_cc::make is the public interface for
       * creating new instances.
       */
      static sptr make(
          double samp_rate,
          double center_freq,
          char tle[240],
          std::vector<double> obs,
          bool use_tags);

      virtual void set_samp_rate(double samp_rate) = 0;
      virtual double samp_rate(void) = 0;

      virtual void set_center_freq(double center_freq) = 0;
      virtual double center_freq(void) = 0;

      /*!
       * \brief Set the satellite TLE.
       *
       * \param tle Two-Line Element set as a three-row string (with satellite name row).
       */
      virtual void set_tle(char tle[240]) = 0;
      /*!
       * \brief Get the satellite TLE.
       *
       * \returns Two-Line Element set as a three-row string (with satellite name row).
       */
      virtual char *tle(void) = 0;

      /*!
       * \brief Set the observer location.
       *
       * \param obs (lat, lon, alt) vector, in signed decimal degrees and metres.
       */
      virtual void set_obs(std::vector<double> obs) = 0;
      /*!
       * \brief Get the observer location.
       *
       * \returns (lat, lon, alt) vector, in signed decimal degrees and metres.
       */
      virtual std::vector<double> obs(void) = 0;

      /*!
       * \brief Set the use_tags flag.
       * 
       * \param use_tags True or False.
       * 
       * If this flag is True, the block looks for rx_time timestamps from UHD to calculate the
       * correct doppler shift. It assumes that the USRP device epoch is set to the UNIX epoch.
       * Otherwise it uses the system clock. Note that the time when work() is called is non-
       * deterministic and thus using system clock introduces frequency jitter.
       */
      virtual void set_use_tags(bool use_tags) = 0;
      /*!
       * \brief Return the current value of the use_tags flag.
       */   
      virtual bool use_tags(void) = 0;

    };

  } // namespace doppler
} // namespace gr

#endif /* INCLUDED_DOPPLER_DOPPLER_TLE_CC_H */

