INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_DOPPLER doppler)

FIND_PATH(
    DOPPLER_INCLUDE_DIRS
    NAMES doppler/api.h
    HINTS $ENV{DOPPLER_DIR}/include
        ${PC_DOPPLER_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    DOPPLER_LIBRARIES
    NAMES gnuradio-doppler
    HINTS $ENV{DOPPLER_DIR}/lib
        ${PC_DOPPLER_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(DOPPLER DEFAULT_MSG DOPPLER_LIBRARIES DOPPLER_INCLUDE_DIRS)
MARK_AS_ADVANCED(DOPPLER_LIBRARIES DOPPLER_INCLUDE_DIRS)

