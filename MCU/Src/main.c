/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2016 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* USER CODE BEGIN Includes */
#include "main.h"
#include "user.h"

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim1;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM1_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void wait_for_PB(void);
int sync_next_bit(void);
int access_next_bit(void);
int packetlen_next_bit(void);
int regstate_next_bit(void);
int PRBS9_next_bit(void);
int zeros_next_bit(void);
void output_bit(GPIO_PinState);

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM1_Init();

  /* USER CODE BEGIN 2 */
  HAL_GPIO_WritePin(LD4_Blue_GPIO_Port, LD4_Blue_Pin, GPIO_PIN_SET);
  HAL_TIM_Base_Start_IT(&htim1);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL3;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0);

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 2);
}

/* TIM1 init function */
void MX_TIM1_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 0;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = __TIM_RL;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  HAL_TIM_Base_Init(&htim1);

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig);

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_ENABLE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig);

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __GPIOD_CLK_ENABLE();
  __GPIOA_CLK_ENABLE();
  __GPIOB_CLK_ENABLE();
  __GPIOC_CLK_ENABLE();

  /*Configure GPIO pin : UserBTN_Pin */
  GPIO_InitStruct.Pin = UserBTN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(UserBTN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PRBS_OUT_Pin */
  GPIO_InitStruct.Pin = PRBS_OUT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_MEDIUM;
  HAL_GPIO_Init(PRBS_OUT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : HCLK_OUT_Pin */
  GPIO_InitStruct.Pin = HCLK_OUT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_MEDIUM;
  HAL_GPIO_Init(HCLK_OUT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LD3_Green_Pin */
  GPIO_InitStruct.Pin = LD3_Green_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
  HAL_GPIO_Init(LD3_Green_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LD4_Blue_Pin */
  GPIO_InitStruct.Pin = LD4_Blue_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
  HAL_GPIO_Init(LD4_Blue_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(PRBS_OUT_GPIO_Port, PRBS_OUT_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(HCLK_OUT_GPIO_Port, HCLK_OUT_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, LD3_Green_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD4_Blue_GPIO_Port, LD4_Blue_Pin, GPIO_PIN_SET);

}

/* USER CODE BEGIN 4 */
static uint16_t reg = 0x0001;  //State of the shift register

void output_bit(GPIO_PinState state)
{
  HAL_GPIO_WritePin(PRBS_OUT_GPIO_Port, PRBS_OUT_Pin, state); // Write out to GPIO
}

int sync_next_bit(void)
{
  static int count = 0;
  
  output_bit( g_syncword[count++] ? GPIO_PIN_SET : GPIO_PIN_RESET );
  
  if ( count >= g_syncword_len )
  {
    count = 0;
    return 1;
  }
  else return 0;
}

int access_next_bit(void)
{
  static uint8_t count = 0;
  
  output_bit( g_access[count++] ? GPIO_PIN_SET : GPIO_PIN_RESET );
  if (count >= g_access_len)
  {
    count = 0;
    return 1;
  }
  else return 0;
}

int packetlen_next_bit(void)
{
  static uint8_t count = 0;
  
  output_bit( g_packetlen[count++] ? GPIO_PIN_SET : GPIO_PIN_RESET );
  if (count >= g_packetlen_len)
  {
    count = 0;
    return 1;
  }
  else return 0;
}

int regstate_next_bit(void)
{
  static int count = 0;
  static uint16_t regstate = 0;  /*< Actual register will never have 0. */
  if ( !count )  regstate = reg;
  
  output_bit( (regstate & (1<<15)) ? GPIO_PIN_SET : GPIO_PIN_RESET );
  regstate <<= 1;
  
  if ( (++count) >= 16)
  {
    count = 0;
    regstate = 0; //Will be 0 anyway, as all bits have been shifted out.
    return 1;
  }
  else return 0;
}

int PRBS9_next_bit(void)
{
  static uint16_t count = 0;
  output_bit(reg&1 ? GPIO_PIN_SET : GPIO_PIN_RESET);
  
  /* Update the state */
  int newbit = (((reg >> 8) ^ (reg >> 4)) & 0x0001); //x^9+x^5+1, PRBS9
  reg = ((reg << 1) | newbit) & 0x01FF;
  
  if (++count >= 256*8)
  {
    count = 0;
    return 1;
  }
  else return 0; 
}
  
int zeros_next_bit(void)
{
  static int count = 0;
  
  output_bit(GPIO_PIN_RESET);
  
  if ( ++count >= ZEROS )
  {
    count = 0;
    return 1;
  }
  else
  {
    return 0;
  }
}

/* 
Output the frame in sections. If called function returns 1, go to next state.

syncword is used for demodulation. 
Access code and packetlen are used by the packet deframer, but really we could
do without, as syncword should be enough, and our packet has constant structure.
Regstate (twice) follows, so if we lose a packet, we can detect it and resync.
PRBS9 outputs the actual test bits.
zeros is padded to the end.
*/
void output_next_bit(void)
{
  static int clock = 0;
  static int state = 0;
  static int (*state_funcs[7])() = {
    sync_next_bit,
    access_next_bit,
    packetlen_next_bit,
    regstate_next_bit,
    regstate_next_bit, /* Register twice, lest we calculate based on wrong register value */
    PRBS9_next_bit,
    zeros_next_bit
  };
  /* On falling edge of clock */
  if (clock)
  {
    /* If the corresponding function returns true, move to the next state */
    if ( state_funcs[state]() ) state++;
    
    if ( state >= 7 ) state = 0;
  }
  HAL_GPIO_TogglePin(HCLK_OUT_GPIO_Port, HCLK_OUT_Pin);  // Output clock.
  clock = !clock;
}

void wait_for_PB(void)
{
  while(HAL_GPIO_ReadPin(UserBTN_GPIO_Port, UserBTN_Pin))
  {} //wait for button to be released.
  HAL_Delay(10); //Debounce delay 10 ms
  while(!HAL_GPIO_ReadPin(UserBTN_GPIO_Port, UserBTN_Pin))  // Wait for button to be pressed
  {
    HAL_Delay(10);
    if(HAL_GPIO_ReadPin(UserBTN_GPIO_Port, UserBTN_Pin))
    {
      break;
    }
  }
}

/* USER CODE END 4 */

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
