#ifndef __MAIN_H
#define __MAIN_H

#define ZEROS 16*8  /* 16 bytes of 0-s. */ 

/*! Syncword to use. */
const int g_syncword[] = {0,0,1,1,0,0,0,1,1,1,1,1,1,0,1,0,1,0,1,1,0,1,1,0};
const int g_syncword_len = sizeof (g_syncword) / sizeof g_syncword[0];

/*! Access code to use. */
const int g_access[] = {0,0,1,1,0,0,1,1,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,1,0,1,1,0,1,0,1,0,1,0};  
const int g_access_len = sizeof (g_access) / sizeof g_access[0];

/*! length field, 256 + 2*2 (for the regstate). 32 bits by definition
(2*uint16_t, as expected by the packet deinterleaver).
Hope this long row of 0-s doesn't cause sync issues...*/
const int g_packetlen[32] = {0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,
                             0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0};
const int g_packetlen_len = 32;

#endif /* __MAIN_H */