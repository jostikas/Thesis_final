from __future__ import print_function
from matplotlib import pyplot as plt
import numpy as np

ax = plt.subplot(111, polar=True)
theta=1
ray = ax.plot((1, 1), (0, 0.7), 'o-', linewidth=3)

totheta = np.linspace(0, theta)
r = np.ones(50)*0.2

print(len(totheta), len(r))
ax.plot(totheta, r, lw=3)

ax.set_rmax(1)
for label in ax.get_xticklabels() + ax.get_yticklabels():
        label.set_visible(False)

plt.text(0.3, 0.07, "$\\theta$", size=25, color="green")
plt.text(theta+0.3, 0.4, "A", size=22, color="blue")
plt.text(0, 1.1, "I", size=30, va="center", ha="center")
plt.text(np.pi/2, 1.1, "Q", size=30, va="center", ha="center")

plt.savefig("polar.eps", format="eps")


plt.show()