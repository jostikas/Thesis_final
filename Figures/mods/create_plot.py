#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function
import sys

reload(sys)
sys.setdefaultencoding("utf-8")

"""
Plot data
"""
import numpy as np
from matplotlib import pyplot as plt

def plot():

    sig = np.fromfile(open("sig.f32"), dtype=np.float32)
    pm = np.fromfile(open("pm.f32"), dtype=np.float32)
    fm = np.fromfile(open("fm.f32"), dtype=np.float32)
    am = np.fromfile(open("am.f32"), dtype=np.float32)
    
    plt.title("Modulatsioonitüübid")
    #plt.text(len_sig/2.0, 0.12, "Modulatsioonitüübid", fontsize=17, ha="center") 
    ax = plt.subplot(111)    
    ax.spines["top"].set_visible(False)    
    ax.spines["bottom"].set_visible(False)    
    ax.spines["right"].set_visible(False)    
    ax.spines["left"].set_visible(False) 

    ax.get_xaxis().tick_bottom()    
    ax.get_yaxis().tick_left()

    ax.set_xlabel("$Aeg$", fontsize=16)
    ax.set_ylabel("$Amplituud$", fontsize=16)

    ax.set_axisbelow(True)

    pos1 = ax.get_position() # get the original position 
    pos2 = [pos1.x0, pos1.y0 + 0.03,  pos1.width, pos1.height] 
    ax.set_position(pos2) # set a new position

    #plt.xlim(0, len(results[0]))  #linear axes are handled okay.
    #plt.ylim(1e-6, 1e-1)
    plt.yticks(fontsize=14)
    plt.xticks(fontsize=14)

    major_ticks = np.arange(0, len(sig), 32000/200)
    ax.set_xticks(major_ticks)
    plt.grid(b=True, axis="x", which='major', color='#aaaaaa', linestyle='--', linewidth=1, )
    #plt.grid(b=True, which='minor', color='#dddddd', linestyle='-', linewidth=1)

    plt.tick_params(axis="both", which="both", bottom="off", top="off",    
                labelbottom="off", left="off", right="off", labelleft="off") 

    sig_plot, = plt.plot(sig,
                   'b-',
                   linewidth=3, 
                   label=u"Signaal")
    am_plot, = plt.plot(am-1,
                   'r-',
                   linewidth=1, 
                   label=u"AM")
    fm_plot, = plt.plot(fm-3,
                   'g-',
                   linewidth=1, 
                   label=u"FM")
    pm_plot, = plt.plot(pm-5,
                   'k-',
                   linewidth=1, 
                   label=u"PM")

    plt.legend(handles=[sig_plot, am_plot, fm_plot, pm_plot])

    #plt.axis("off")  # Väga barebones.
    fname = 'yle-mods'
    plt.savefig(fname + ".eps", format='eps')
    plt.savefig(fname + ".png", format='png')

    plt.show()


if __name__ == "__main__":

    plot()

