#!/usr/bin/python
from __future__ import print_function

"""Take an impulse response and output the matched filter"""

import argparse
import warnings
from os.path import splitext
import numpy as np
from re import search
from matplotlib import pyplot as pl
from Thesis.utils import show_plot, cumphase

def window_rms(pulse, window_size):
    """Windowed RMS over pulse.

    Credits to matehat from http://stackoverflow.com/a/8260297/3745323
    """
    sqr = np.square(np.abs(pulse))
    window = np.ones(window_size)/float(window_size)
    rms = np.sqrt(np.convolve(sqr, window, 'same'))
    return rms

def raise_pulse(pulse):
    """Raise the real part of the pulse to get sharp RMS peak."""
    real_ends = np.append([],(pulse.real[:50], pulse.real[-50:]))
    mean = np.mean(real_ends)
    raised = (pulse.real - mean) + pulse.imag*1j
    return raised

def center_pulse(pulse, symbol_time, verbose=0):
    """Center the pulse in the array"""
    ## Find the center of the pulse by searching for power peak
    raised = raise_pulse(pulse)  #Raise by shifting the real part
    rms = window_rms(raised, int(symbol_time))
    if verbose >= 3:
        show_plot(rms.real, 'RMS')
    peak = rms.argmax()
    ## Shift it to the center of the array
    center = len(pulse)/2
    shift = center - peak
    pulse = np.roll(raised, shift)
    #pulse = np.roll(pulse, shift)
    return pulse

def pad_pulse(pulse, symbol_time, N=7, verbose=0):
    """Pad the pulse to N symbols length"""
    final = symbol_time*N
    current = len(pulse)
    padding = int((final-current) / 2)
    if verbose:
        print("Padding with {} values on both sides.".format(padding))
    if padding > 0:
        pulse = np.pad(pulse, padding/2, mode='mean', stat_length=(50,))
    elif padding < 0:
        pulse = pulse[-padding/2:padding/2]
    return pulse

def norm_pulse(pulse, verbose=0):
    """Normalize the pulse."""
    pwr = np.sum(np.abs(pulse))
    if verbose:
        print("Pulse power: {}".format(pwr))
    norm = pulse/pwr
    return norm

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Will complex-conjugate and normalize a pulse from input file.")

    parser.add_argument('files', type=str, nargs='+', help='Files to process')
    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help='Dump more info, show graph')
    parser.add_argument('-r', '--rate', dest='rate', type=float, default=None, help="Symbol rate of the file.")
    parser.add_argument('-s', '--samp_rate', dest='samp_rate', type=float, default=25e6/7, help="Sample rate of the file.")
    parser.add_argument('-N', '--filt_width', dest='N', type=int, default=7, help='Resulting filter width in symbols. Should be odd.')

    args = parser.parse_args()

    for file_name in args.files:
        out_name = '-filter-complex'.join(splitext(file_name))

        if args.rate:
            rate = args.rate  # If rate is defined by arguments
        else:  # Otherwise, determine from filename (must contain the '-r100' group)
            # Regexp searches for a float between '-r' and either '-' or '.'
            match = search(r'-r(\d+(\.\d*)?)[-.]', file_name)
            if match:
                rate = float(match.group(1))*1000
                if args.verbose:
                    print("Rate: {} for file {}.".format(rate, file_name))
            else:
                print("Rate couldn't be determined for filename {}".format(file_name))
                continue  # Abort this iteration, continue with next file

        with open(file_name) as file:  # Read pulse from file
            pulse = np.fromfile(file, dtype=np.complex64)
        if args.verbose >= 2:
            show_plot(pulse, 'Original pulse')

        filt = pulse.conjugate()  # Conjugate the 1D array
        if args.verbose >= 3:
            show_plot(filt, 'Conjugated pulse')

#        filt = raise_pulse(filt)  # Raise and normalize the pulse
#        if args.verbose >= 3:
#            show_plot(filt, 'Raised and normalized pulse')

        symbol_time = args.samp_rate/rate
        if args.verbose:
            print('Symbol time: {}'.format(symbol_time))
        filt = center_pulse(filt, symbol_time, verbose=args.verbose)
        if args.verbose >= 2:
            show_plot(filt, 'Shifted and conjugated pulse')

        filt = pad_pulse(filt, symbol_time, N=args.N, verbose=args.verbose)
        if args.verbose >= 2:
            show_plot(filt, 'Padded pulse')

        filt = norm_pulse(filt, args.verbose)
        if args.verbose >= 1:
            show_plot(filt, "Normalised filter")

        filt.tofile(out_name)
        if args.verbose:
            print("Saved filter to file {}.".format(out_name))
    print("Done.")
