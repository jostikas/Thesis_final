#!/usr/bin/python
import psync_pulses
import numpy as np
from Thesis.utils import show_plot, fshift_pulses, match_ends


def extract_and_average(src_filename, samp_rate = 25e6/7, c_freq = -66200, verbose=0, store_fig=False, max_pulses=10):
    """Extract and average pulses from recorded pulse train files.

    @param src_filename: File name to process
    @param samp_rate: Sample rate. Default is 25 MHz/7
    @param c_freq: Approximate center frequency of the signal. Defaults to -66200 Hz
    @param verbose: 0 - Show no graphs. 1-Show final pulse. 2-Show synced stream. 3-Show all intermediate files.
    """

    ## Set filenames.
    src = src_filename.rsplit('.', 1)
    fshift_name = '-shifted.'.join(src)
    match_name = '-matched.'.join(src)
    psync_name = '-synced.'.join(src)
    mean_name = '-mean.'.join(src)
    png_name = '-mean.'.join((src[0], 'png'))
    svg_name = '-mean.'.join((src[0], 'svg'))

    ## Plot input data
    if verbose >= 3:
        outdata = np.fromfile(open(src_filename), dtype=np.complex64)
        show_plot(outdata, "Input data")

    ## Shift the signal frequency, and low-pass filter. Filter cutoff defined in fshift_pulses.py
    fshift = fshift_pulses(center_freq=c_freq,
                           f_in=src_filename,
                           f_out=fshift_name,
                           lpf=True
                           )
    fshift.run()

    ## Plot shifted and filtered signal
    if verbose >= 3:
        outdata = np.fromfile(open(fshift_name), dtype=np.complex64)
        show_plot(outdata, "Shifted and filtered data")

    ## Match the phase of the ends of the signal
    outdata = np.fromfile(open(fshift_name), dtype=np.complex64)
    outdata = match_ends(outdata)
    outdata.tofile(match_name)

    ## Plot
    if verbose >= 3:
        show_plot(outdata, 'Matched data')

    ## Phase synchronization via Costas loop
    psync = psync_pulses.psync_pulses(match_name, psync_name)
    psync.run()
    outdata = np.fromfile(psync_name, dtype=np.complex64)

    ## Plot the synced data
    if verbose >= 2:
        show_plot(outdata, "Synced data")

    ## Geenrate a trigger signal
    trigger = outdata.real < 0
    trigger = np.diff(trigger)

    ## Extract the edge indices
    edges = trigger.nonzero()[0]
    if verbose >= 3:
        print 'Edge locations: {}'.format(edges)

    ## Create ndarray for storing the individual pulses
    pulses = np.zeros((max_pulses, 2e-3*samp_rate), dtype=np.complex64)

    ## Set up variables for loop
    next = 0  # Next edge to use
    count = 0  # Total detected pulses

    for row in range(max_pulses):
        ## Chunk off 2ms segments
        start = int(edges[next] - 0.5e-3*samp_rate)
        stop = int(start + 2e-3*samp_rate)
        pulses[row][:] = outdata[start:stop]
        count += 1

        ## Find the next edge
        try:
            while edges[next] < stop:
                next += 1
        except IndexError:
            break  # out of pulses. Maybe we missed some.

    ## Create storage for the mean, calculate the mean (with complex128 accumulator)
    mean = np.ndarray(2e-3*samp_rate, dtype=np.complex64)
    np.mean(pulses[:count], axis=0, dtype=np.complex128, out=mean)

    if verbose >= 1 or store_fig:
        pl = show_plot(mean, 'Final pulse', instance_only=True)
        if store_fig:
            pl.savefig(png_name, format='png', transparent=False)
            pl.savefig(svg_name, format='svg', transparent=True)
        if verbose >= 1:
            pl.show()


    ## Output to file
    mean.tofile(mean_name)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="Extract and average pulses from the pulse train.")

    parser.add_argument('files', type=str, nargs='+', help="Files to process.")
    parser.add_argument('-f','--c_freq', dest='c_freq', type=float, default=-66000.0,
                        help="Approximate center frequency of the signal."
                        )
    parser.add_argument('-s', '--samp_rate', dest='samp_rate', type=float, default=25e6/7,
                        help="Sampling rate of the file. Used for absolute frequency values."
                        )
    parser.add_argument('-v', dest='verbose', action='count',
                        help="Dump more info. Add 'v'-s for victory.")
    parser.add_argument('-i', '--store_fig', dest='store_fig', action='store_true', default=False)
    parser.add_argument('-p', '--max_pulses', dest='max_pulses', type=int, default=10, help="Max number of pulses to average.")

    args = parser.parse_args()
    print args.files
    for src_filename in args.files:
        extract_and_average(src_filename, args.samp_rate, args.c_freq, args.verbose, args.store_fig, args.max_pulses)
        print "Processed {}".format(src_filename)
    print "Done."
