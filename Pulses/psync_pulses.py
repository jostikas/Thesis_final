#!/usr/bin/env python2
##################################################
# GNU Radio Python Flow Graph
# Title: Phase sync pulses
# Author: Laur Joost
# Generated: Mon Mar 14 17:41:45 2016
##################################################

from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser


class psync_pulses(gr.top_block):

    def __init__(self, f_in='', f_out=''):
        gr.top_block.__init__(self, "Phase sync pulses")

        ##################################################
        # Parameters
        ##################################################
        self.f_in = f_in
        self.f_out = f_out

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 25e6/7

        ##################################################
        # Blocks
        ##################################################
        self.digital_costas_loop_cc_0 = digital.costas_loop_cc(200e-6, 2, False)
        self.blocks_file_source_0 = blocks.file_source(gr.sizeof_gr_complex*1, f_in, False)
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_gr_complex*1, f_out, False)
        self.blocks_file_sink_0.set_unbuffered(False)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_file_source_0, 0), (self.digital_costas_loop_cc_0, 0))    
        self.connect((self.digital_costas_loop_cc_0, 0), (self.blocks_file_sink_0, 0))    


    def get_f_in(self):
        return self.f_in

    def set_f_in(self, f_in):
        self.f_in = f_in
        self.blocks_file_source_0.open(self.f_in, False)

    def get_f_out(self):
        return self.f_out

    def set_f_out(self, f_out):
        self.f_out = f_out
        self.blocks_file_sink_0.open(self.f_out)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate


if __name__ == '__main__':
    parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
    parser.add_option("", "--f-in", dest="f_in", type="string", default='',
        help="Set input file [default=%default]")
    parser.add_option("", "--f-out", dest="f_out", type="string", default='',
        help="Set Output file [default=%default]")
    (options, args) = parser.parse_args()
    tb = psync_pulses(f_in=options.f_in, f_out=options.f_out)
    tb.start()
    tb.wait()
