#!/usr/bin/python
"""
Generate a stream of doppler shifted constant for comparison with gpredict.
Export results as csv
"""


from gnuradio import gr
from gnuradio import blocks, analog
import doppler
import pmt
import numpy as np

import csv

from setup import starttime

#! TLE from http://celestrak.com/NORAD/elements/amateur.txt on May 15, 2016
TLE = """EYESAT-1 (AO-27)        
1 22825U 93061C   16137.10783469  .00000044  00000-0  34666-4 0  9991
2 22825  98.7565  93.1611 0009392  55.9881 304.2192 14.29954377180653"""

params = dict(
              samp_rate=4000.0,
              center_freq=100e6,  # As that is what Gpredict outputs it's estimates at.
              obs=(58.39097, 26.62874, 70.0),
              use_tags=True,  # Set to tag-synchronous mode.
              tle=TLE
              )

samp_rate = params['samp_rate']

#! Give the starttime to the doppler block with the first sample.
tags = []
tags.append(gr.tag_utils.python_to_tag(
    (0, 
    pmt.intern("rx_time"),
    pmt.to_pmt((starttime, 0)),
    pmt.intern("src")
    ))
)
#Simulate packet drop at 1000 samples, with a loss of 100 samples.
ts = starttime+1100/samp_rate
timespec = divmod(ts, 1)
tags.append(gr.tag_utils.python_to_tag(
    (1000, 
    pmt.intern("rx_time"),
    pmt.to_pmt((int(timespec[0]), timespec[1])),
    pmt.intern("src")
    ))
)
#Simulate packet drop at 1200 samples, with a loss of 100 samples.
ts = starttime+1400/samp_rate
timespec = divmod(ts, 1)
tags.append(gr.tag_utils.python_to_tag(
    (1200, 
    pmt.intern("rx_time"),
    pmt.to_pmt((int(timespec[0]), timespec[1])),
    pmt.intern("src")
    ))
)
#Simulate packet drop at 1202 samples, with a loss of 100 samples.
ts = starttime+1502/samp_rate
timespec = divmod(ts, 1)
tags.append(gr.tag_utils.python_to_tag(
    (1202, 
    pmt.intern("rx_time"),
    pmt.to_pmt((int(timespec[0]), timespec[1])),
    pmt.intern("src")
    ))
)
#If source stream has been decimated, then tags may end up on the same samples.
#Simulate packet drop at 1300 samples, with a loss of 100 samples.
ts = starttime+1700/samp_rate
timespec = divmod(ts, 1)
tags.append(gr.tag_utils.python_to_tag(
    (1300, 
    pmt.intern("rx_time"),
    pmt.to_pmt((int(timespec[0]), timespec[1])),
    pmt.intern("src")
    ))
)
#Simulate packet drop at 1300 samples, with a loss of 100 samples.
ts = starttime+1800/samp_rate
timespec = divmod(ts, 1)
tags.append(gr.tag_utils.python_to_tag(
    (1300, 
    pmt.intern("rx_time"),
    pmt.to_pmt((int(timespec[0]), timespec[1])),
    pmt.intern("src")
    ))
)


def run_and_save():

    ## Set up the flowgraph
    tb = gr.top_block()

    src_vect = 2000*[1,]
    src = blocks.vector_source_c(src_vect, False, 1, tags)
    dop = doppler.doppler_tle_cc(**params)
    qdm = analog.quadrature_demod_cf(samp_rate / (2*np.pi))  #< This results in instantaneous frequency in Hz
    # We'll only keep the first sample per second.
    dst = blocks.file_sink(gr.sizeof_float*1, "block_output_frequency.f32", False)
    dst.set_unbuffered(False)

    tb.connect(src, dop, qdm, dst)
    tb.run()
    
if __name__ == '__main__':
    run_and_save()