#!/usr/bin/python
"""
Generate a stream of doppler shifted constant for comparison with gpredict.
Export results as csv
"""


from gnuradio import gr
from gnuradio import blocks, analog
import doppler
import pmt
import numpy as np

import csv

from setup import starttime, stoptime

#! Give the starttime to the doppler block with the first sample.
tag = gr.tag_utils.python_to_tag((0, pmt.intern("rx_time"), pmt.to_pmt((starttime, 0)), pmt.intern("src")))

#! TLE from http://celestrak.com/NORAD/elements/amateur.txt on May 15, 2016
TLE = """EYESAT-1 (AO-27)        
1 22825U 93061C   16137.10783469  .00000044  00000-0  34666-4 0  9991
2 22825  98.7565  93.1611 0009392  55.9881 304.2192 14.29954377180653"""

params = dict(
              samp_rate=4000,
              center_freq=100e6,  # As that is what Gpredict outputs it's estimates at.
              obs=(58.39097, 26.62874, 70.0),
              use_tags=True,  # Set to tag-synchronous mode.
              tle=TLE
              )

def run_and_save():
    samp_rate = params['samp_rate']

    ## Set up the flowgraph
    tb = gr.top_block()

    src = blocks.vector_source_c((1,), True, 1, [tag])
    ## To avoid having the tag associated with every sample, or having to construct the whole array in memory,
    ## we'll use repeat: will use interpolation rules for tag handling, won't repeat the tag.
    len = int((stoptime - starttime)*samp_rate)
    print len
    rep = blocks.repeat(gr.sizeof_gr_complex*1, len)
    head = blocks.head(gr.sizeof_gr_complex*1, len)  #Limit time.
    dop = doppler.doppler_tle_cc(**params)
    qdm = analog.quadrature_demod_cf(samp_rate / (2*np.pi))  #< This results in instantaneous frequency in Hz
    # We'll only keep the first sample per second.
    dec = blocks.keep_one_in_n(gr.sizeof_float*1, samp_rate)
    dst = blocks.file_sink(gr.sizeof_float*1, "block_output_frequency.f32", False)
    dst.set_unbuffered(False)

    tb.connect(src, rep, head, dop, qdm, dec, dst)
    tb.run()
    
if __name__ == '__main__':
    run_and_save()