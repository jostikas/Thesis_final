#!/usr/bin/python
"""
Extract doppler values and time values from gpredict pass file.

Converts the time values into UNIX timestamp, stores timestamp-doppler
pairs in dicts in a list.
"""

from setup import passfile, blockfile, starttime, stoptime
from time import strptime
from calendar import timegm
from os.path import splitext

gpoutfile = splitext(passfile)[0]+'.csv'
bloutfile = splitext(blockfile)[0]+'.csv'

import csv
import numpy


def fromgpredict(passfile=passfile):
    with open(passfile, "rb") as pf:
        predictions = []
        reader = csv.DictReader(pf, 
                                ["date", "time", "dop"],
                                delimiter=" ", 
                                skipinitialspace=True)
        for row in reader:
            dt = strptime(" ".join((row['date'], row['time'])),
                          "%Y/%m/%d %H:%M:%S")
            time = timegm(dt)
            predictions.append((float(time), float(row['dop'])))
        return predictions

def fromdoppler(blockfile=blockfile):
    dops = numpy.fromfile(open(blockfile), dtype=numpy.float32).astype(numpy.double)
    ts = numpy.arange(starttime, stoptime, dtype=numpy.double)

    arr = numpy.column_stack((ts, dops))
    preds = fromnumpy(arr)
    return preds

def fromnumpy(array):
    return array.tolist()

def tocsv(predictions, outfile):
    with open(outfile, 'wb') as of:
        writer = csv.writer(of)
        writer.writerow(("timestamp", "doppler"))
        writer.writerows(predictions)

def tonumpy(predictions):
    arr = numpy.asarray(predictions, dtype=numpy.double)
    return arr

if __name__ == "__main__":
    """Convert gpredict and doppler results"""
    preds = fromgpredict()
    tocsv(preds, gpoutfile)

    preds = fromdoppler(blockfile)
    tocsv(preds, bloutfile)



