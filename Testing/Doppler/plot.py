# -*- coding: utf-8 -*-
from __future__ import print_function, unicode_literals
from matplotlib import pyplot as plt
from setup import starttime, stoptime, blockfile, passfile
import extract
import numpy as np

block = extract.fromdoppler(blockfile)
gpred = extract.fromgpredict(passfile)

block = np.asarray(block).T
gpred = np.asarray(gpred).T

dop_plot, = plt.plot(block[0]-gpred[0][0], block[1], '-', label="Mõõdetud", lw=1.5)
gpred_plot, = plt.plot(gpred[0]-gpred[0][0], gpred[1], '+', markersize=10, label="GPredict", mew=1.5)

plt.legend((dop_plot, gpred_plot), ["Mõõdetud", "GPredict"])

plt.title("SNR ploki kalibratsioon")

ax = plt.subplot(111)    
ax.spines["top"].set_visible(False)    
ax.spines["bottom"].set_visible(False)    
ax.spines["right"].set_visible(False)    
ax.spines["left"].set_visible(False) 

ax.get_xaxis().tick_bottom()    
ax.get_yaxis().tick_left()

ax.set_xlabel("$Aeg\ ülelennu\ algusest,\ s$", fontsize=16)
ax.set_ylabel("$Sagedus,\ Hz$", fontsize=16)

ax.set_axisbelow(True)

pos1 = ax.get_position() # get the original position 
pos2 = [pos1.x0+0.02, pos1.y0 + 0.03,  pos1.width, pos1.height] 
ax.set_position(pos2) # set a new position

plt.xlim(-20, 620)  #linear axes are handled okay.
#plt.ylim(1e-6, 1e-1)
plt.yticks(fontsize=14)
plt.xticks(fontsize=14)

plt.grid(b=True, axis="both", which='major', color='#dddddd', linestyle='--', linewidth=1, )
#plt.grid(b=True, which='minor', color='#dddddd', linestyle='-', linewidth=1)

plt.tick_params(axis="both", which="both", bottom="off", top="off",    
            labelbottom="on", left="off", right="off", labelleft="on")

fname = 'test-doppler'
plt.savefig(fname + ".eps", format='eps')
plt.savefig(fname + ".png", format='png')

plt.show()
