from __future__ import print_function

import numpy as np

snrs = []
with open("caldata/noise_est_pure_1", "r") as f:
    for line in f:
        if not line.startswith("*"):
            snrs.append(float(line))

snr1 = np.mean(snrs)
sig1 = np.std(snrs)

snrs=[]

with open("caldata/noise_est_pure_2", "r") as f:
    for line in f:
        if not line.startswith("*"):
            snrs.append(float(line))

snr2 = np.mean(snrs)
sig2 = np.std(snrs)/np.sqrt(len(snrs))

snr = np.mean((snr1, snr2))
sig = np.mean((sig1, sig2))  # not random variables.

print("snr: {}, sigma: {}".format(snr, sig))

frac = 1.0/(10**(snr/10) + 1)
s_frac = np.sqrt(((snr*10**(snr/10 -1))/(10**(snr/10))**2)**2 * sig**2)

print("frac: {}, s_frac = {}".format(frac, s_frac))

