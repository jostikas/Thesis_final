#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function

"""
Extract BER and SNR data from BER logs, process and output to csv.
"""
import numpy as np
import scipy
from scipy import stats
from matplotlib import pyplot as plt

def extract(filename):
    intercept, slope, s_int, s_slo = np.fromfile(open("cal-data.f32"),
        dtype=np.float32).tolist()

    snrs = []
    tot = 0
    be = 0
    with open(filename, "r") as f:
        for line in f:
            if line.startswith("BE :"):
                be = int(line[4:])  # Only keep last.
            elif line.startswith("BTS:"):
                tot = float(line[4:])
            elif line.startswith("SNR:"):
                snrs.append(float(line[4:]))

        snr = intercept + slope*np.mean(snrs)
        snr_sigma = np.sqrt(slope**2 * np.std(snrs, ddof=1)**2 +
                            np.mean(snrs)**2 * s_slo**2 +
                            s_int**2 +
                            2*slope*np.mean(snrs)*s_slo*np.std(snrs, ddof=1))

        snr_conf = stats.norm.interval(0.95, 0, snr_sigma / np.sqrt(len(snrs)))

        # Check fit
        #n, bins, patches = plt.hist(snrs, int(np.sqrt(len(snrs))), normed=True)
        #x = np.linspace(snr_conf[0]*len(snrs), snr_conf[1]*len(snrs)) + snr
        #print(x)
        #y = stats.norm.pdf( x, snr, snr_sigma)
        #l = plt.plot(x, y, 'r--', linewidth=1)
        #plt.show()

        ber = be/tot
        ber_conf = []
        be_conf = stats.poisson.interval(0.95, be)
        ber_conf.append(ber - be_conf[0] / tot)  # Need those values relative to data
        ber_conf.append(be_conf[1] / tot - ber)
        return (snr, ber, snr_conf[1], ber_conf[0], ber_conf[1])

def plot(results):
    
    plt.text(6.5, 0.12, "Bitivigade tihedus", fontsize=17, ha="center") 
    ax = plt.subplot(111)    
    ax.spines["top"].set_visible(False)    
    ax.spines["bottom"].set_visible(False)    
    ax.spines["right"].set_visible(False)    
    ax.spines["left"].set_visible(False) 

    ax.get_xaxis().tick_bottom()    
    ax.get_yaxis().tick_left()

    ax.set_xlabel("$E_b / No,\, dB$", fontsize=16)
    ax.set_ylabel("$BER$", fontsize=16)

    ax.set_axisbelow(True)

    pos1 = ax.get_position() # get the original position 
    pos2 = [pos1.x0, pos1.y0 + 0.03,  pos1.width, pos1.height] 
    ax.set_position(pos2) # set a new position

    plt.xlim(2,11)
    plt.ylim(1e-6, 1e-1)
    plt.yticks(fontsize=14)
    plt.xticks(fontsize=14)

    plt.grid(b=True, which='major', color='#aaaaaa', linestyle='--', linewidth=1)
    plt.grid(b=True, which='minor', color='#dddddd', linestyle='-', linewidth=1)

    plt.tick_params(axis="both", which="both", bottom="off", top="off",    
                labelbottom="on", left="off", right="off", labelleft="on") 

    na = np.array(results).T
    measured = plt.errorbar(na[0], na[1],
                             yerr=[na[3], na[4]],
                             xerr=na[2], 
                             fmt='o', 
                             label=u"Mõõdetud")
    print(measured)

    theory_x = np.linspace(2,11,100)
    theory_y = 0.5*scipy.special.erfc(np.sqrt((10**(theory_x/10))))
    bpsk, = plt.semilogy(theory_x, theory_y, 'k-', label="Optimaalne", lw=1.5)

    plt.legend(handles=[measured, bpsk])

    fname = 'test-graafik'
    plt.savefig(fname + ".eps", format='eps')
    plt.savefig(fname + ".png", format='png')

    plt.show()


if __name__ == "__main__":
    import csv
    import argparse

    parser = argparse.ArgumentParser(description="Process BER output")
    parser.add_argument("files", type=str, nargs="+")
    parser.add_argument("-o", dest="outfile", default="bers_out.csv", type=str)

    args = parser.parse_args()

    results = []
    for filename in args.files:
        result = extract(filename)
        if float("nan") in result:  # remove nan-containing points, as they casue 
            continue
        results.append(result)
    results.sort(key=lambda x: x[0])

    writer = csv.writer(open(args.outfile, "wb"))
    writer.writerow("snr, ber, snr_conf_low, snr_conf_high, ber_conf_low, ber_conf_high")
    writer.writerows(results)

    plot(results)

