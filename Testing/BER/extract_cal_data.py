# -*- coding: utf-8 -*-
from __future__ import print_function, unicode_literals
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import linregress


"Extract calibration coeffs from caldata."
indices=range(2,12)

# Read data from files.
#divs = [[],[]  # means and sigmas
#snbs = [[],[]]
divs = [[],]
snbs = [[],]
for snr in indices:
    div = np.fromfile(open("caldata/div{}.f32".format(snr)), dtype=np.float32)
#    divs[0].append(np.mean(div))
#    divs[1].append(np.std(div, ddof=1) / np.sqrt(len(div)))
    divs[0].extend(div.tolist())
    with open("caldata/snr_block{}".format(snr)) as snblock:
#        vals = []
        for line in snblock:
            try:
#                vals.append(float(line))
                snbs[0].append(float(line))
            except ValueError:
                pass
#        snbs[0].append(np.mean(vals))
#        snbs[1].append(np.std(vals, ddof=1) / np.sqrt(len(vals)))
print(len(divs[0]))
print(len(snbs[0]))

divs = np.array(divs)
snbs = np.array(snbs)

slope, intercept, r_value, p_value, std_err = linregress(divs[0], snbs[0])

errs = snbs[0] - slope*divs[0] - intercept

# Estimate std of slope and intercept
n = len(divs[0])
num = np.sqrt(np.sum(divs[0]**2) * np.sum(errs**2))
den = (n-2) * (n*np.sum(divs[0]**2) - np.sum(divs[0])**2)
s_int = np.sqrt(num/den)

num = n*np.sum(errs**2)
# den is same.
s_slo = np.sqrt(num/den)

print("Slope: {}, intercept: {}, s_slope: {}, s_inter: {}".format(slope,
                                                                  intercept,
                                                                  s_slo,
                                                                  s_int))

result = np.array((intercept, slope, s_int, s_slo), dtype=np.float32)
result.tofile("cal-data.f32")

## Plot the graph
## First, formatting fluff.
plt.title("SNR ploki kalibratsioon")

ax = plt.subplot(111)    
ax.spines["top"].set_visible(False)    
ax.spines["bottom"].set_visible(False)    
ax.spines["right"].set_visible(False)    
ax.spines["left"].set_visible(False) 

ax.get_xaxis().tick_bottom()    
ax.get_yaxis().tick_left()

ax.set_xlabel("$SNR,\ dB$", fontsize=16)
ax.set_ylabel("$Mõõdetud\ SNR,\ dB$", fontsize=16)

ax.set_axisbelow(True)

pos1 = ax.get_position() # get the original position 
pos2 = [pos1.x0, pos1.y0 + 0.03,  pos1.width, pos1.height] 
ax.set_position(pos2) # set a new position

#plt.xlim(0, len(results[0]))  #linear axes are handled okay.
#plt.ylim(1e-6, 1e-1)
plt.yticks(fontsize=14)
plt.xticks(fontsize=14)

plt.grid(b=True, axis="both", which='major', color='#dddddd', linestyle='--', linewidth=1, )
#plt.grid(b=True, which='minor', color='#dddddd', linestyle='-', linewidth=1)

plt.tick_params(axis="both", which="both", bottom="off", top="off",    
            labelbottom="on", left="off", right="off", labelleft="on")

## Data
reg, = plt.plot(divs[0], intercept+slope*divs[0], 'b')
plt.fill_between(divs[0],
                 intercept+1.96*s_int+(slope+1.96*s_slo)*divs[0],
                 intercept-1.96*s_int+(slope-1.96*s_slo)*divs[0],
                 color='lightblue',
                 lw=0.5)
proxy, = plt.fill(np.NaN, np.NaN, 'lightblue', lw=0)  # Proxy artist for legend
#cal_points = plt.errorbar(divs[0], 
#                          snbs[0],xerr=1.96*divs[1], 
#                          yerr=1.96*snbs[1], 
#                          fmt='r+'
#                          )
cal_points, = plt.plot(divs[0], snbs[0], 'r.')
plt.text(5.5, 5, "-0.01(2) + 1.001(6)x")
plt.legend([(proxy, reg), cal_points],
           ["Lineaarregressioon", "Kalibr. punktid"],
           loc = "best")

fname = 'test-cal'
plt.savefig(fname + ".eps", format='eps')
plt.savefig(fname + ".png", format='png')

plt.show()