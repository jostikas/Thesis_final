"""
Utility functions for working with sample files in python.
"""

import numpy as np
from matplotlib import pyplot as pl
import warnings
from os.path import splitext

def cumphase(sig):
    """
    Get cumulative phase (not -pi...pi wrapped) for a complex sample stream.

    Will process using double as datatype for better precision.

    Update: There IS a way to achieve the same using gnuradio:
    quadrature_demod -> iir_filter(1, (1,-1), oldstyle=False)
    i.e. get phase differences and integrate.

    @arg sig: Complex ndarray, that satisfies Nyquist sampling criteria.
    @returns: ndarray of the same length as sig, with cumulative phase values.
    """
    ang = np.angle(sig).astype(np.double)
    dif = np.diff(ang)
    leaps = np.sign(dif) * (np.abs(dif) > np.pi) + 0.0 # adding 0.0 eliminates -0.0-s
    phase = np.ndarray(len(sig))
    phase[0] = ang[0]
    phase[1:] = np.cumsum(dif - 2*np.pi*leaps) + phase[0]
    return phase

def fft(sig, samp_rate):
    """
    Returns a power spectrum with zero frequency shifted to the middle.
    Also returns bin frequencies.

    @arg sig: signal to process
    @arg samp_rate: Sample rate of the signal, for calculating frequencies.calculating
    @returns: (bins:ndarray, frequencies:ndarray)
    """
    four = np.fft.fftshift(np.fft.fft(sig))
    four = (np.abs(four)**2).astype(np.double)  # Convert to power spectrum
    four /= np.sum(four)  # Normalize spectrum
    four = np.log10(four) * 10  # To dB
    freq = np.fft.fftshift(np.fft.fftfreq(len(sig), 1/samp_rate))
    return (four, freq)

def show_plot(data, title='Unnamed', instance_only=False):
    """Show the plot of input data, complex, if necessary.

    @arg data: Data to show.
    @arg title: Title to put to the plot. Default to 'Unnamed'.
    @arg instance_only: Only return the plot instance, without showing the plot. Default to False.
    """
    x = np.arange(len(data))
    pl.clf()
    with warnings.catch_warnings():
        warnings.filterwarnings('error', category=np.ComplexWarning)
        try:
            pl.plot(x, data, 'b-')
        except np.ComplexWarning:
            pl.plot(x, data.real, 'b-',
                    x, data.imag, 'r-')
    pl.title(title)
    if not instance_only:
        pl.show()
    return pl

def normalize(datafile, dtype=None):
    """Normalize the input file, store in itself.

    \param data: file name to a native binary file
    \param dtype: Data type of the file. By default tries to guess from file extension.
    \return: Normalization gain
    """
    if not dtype:
        ext = splitext(datafile)[1]
        if ext == ".cf32":
            dtype=np.complex64
        elif ext == ".f32":
            dtype = np.float32
        else:
            raise ValueError("Couldn't guess data type.")
    with open(datafile) as dfile:
        d = np.fromfile(dfile, dtype)
    g = np.sum(np.abs(d))
    d /= g
    d.tofile(datafile)
    return g
