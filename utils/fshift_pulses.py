#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Coarse frequency correction
# Author: Laur Joost
# Generated: Wed Apr 20 10:44:11 2016
##################################################

from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser


class fshift_pulses(gr.top_block):

    def __init__(self, center_freq=-66200, f_out='', f_in='', cutoff=2e5, width=5e3, lpf=True):
        gr.top_block.__init__(self, "Coarse frequency correction")

        ##################################################
        # Parameters
        ##################################################
        self.center_freq = center_freq
        self.f_out = f_out
        self.f_in = f_in
        self.cutoff = cutoff
        self.width = width
        self.lpf = lpf
        print f_in, f_out

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 25e6/7
        self.taps = taps = filter.firdes.low_pass(1000, samp_rate, cutoff, width) if lpf else [1,]

        ##################################################
        # Blocks
        ##################################################
        self.freq_xlating_fir_filter_xxx_0 = filter.freq_xlating_fir_filter_ccc(1, (taps), center_freq, samp_rate)
        self.blocks_skiphead_0 = blocks.skiphead(gr.sizeof_gr_complex*1, len(taps)/2)
        self.blocks_file_source_0 = blocks.file_source(gr.sizeof_gr_complex*1, f_in, False)
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_gr_complex*1, f_out, False)
        self.blocks_file_sink_0.set_unbuffered(False)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_file_source_0, 0), (self.freq_xlating_fir_filter_xxx_0, 0))    
        self.connect((self.blocks_skiphead_0, 0), (self.blocks_file_sink_0, 0))    
        self.connect((self.freq_xlating_fir_filter_xxx_0, 0), (self.blocks_skiphead_0, 0))    

    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        self.freq_xlating_fir_filter_xxx_0.set_center_freq(self.center_freq)

    def get_f_out(self):
        return self.f_out

    def set_f_out(self, f_out):
        self.f_out = f_out
        self.blocks_file_sink_0.open(self.f_out)

    def get_f_in(self):
        return self.f_in

    def set_f_in(self, f_in):
        self.f_in = f_in
        self.blocks_file_source_0.open(self.f_in, False)

    def get_cutoff(self):
        return self.cutoff

    def set_cutoff(self, cutoff):
        self.cutoff = cutoff
        self.set_taps(filter.firdes.low_pass(1000, self.samp_rate, self.cutoff, self.width) if self.lpf else [1,])

    def get_width(self):
        return self.width

    def set_width(self, width):
        self.width = width
        self.set_taps(filter.firdes.low_pass(1000, self.samp_rate, self.cutoff, self.width) if self.lpf else [1,])

    def get_lpf(self):
        return self.lpf

    def set_lpf(self, lpf):
        self.lpf = lpf
        self.set_taps(filter.firdes.low_pass(1000, self.samp_rate, self.cutoff, self.width) if self.lpf else [1,])

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_taps(filter.firdes.low_pass(1000, self.samp_rate, self.cutoff, self.width) if self.lpf else [1,])

    def get_taps(self):
        return self.taps

    def set_taps(self, taps):
        self.taps = taps
        self.freq_xlating_fir_filter_xxx_0.set_taps((self.taps))


def argument_parser():
    parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
    parser.add_option(
        "", "--center-freq", dest="center_freq", type="eng_float", default=eng_notation.num_to_str(-66200),
        help="Set Center frequency [default=%default]")
    parser.add_option(
        "", "--f-out", dest="f_out", type="string", default='',
        help="Set Output file [default=%default]")
    parser.add_option(
        "", "--f-in", dest="f_in", type="string", default='',
        help="Set input file [default=%default]")
    parser.add_option(
        "", "--cutoff", dest="cutoff", type="eng_float", default=eng_notation.num_to_str(2e5),
        help="Set LPF cutoff [default=%default]")
    parser.add_option(
        "", "--width", dest="width", type="eng_float", default=eng_notation.num_to_str(5e3),
        help="Set LPF transition width [default=%default]")
    parser.add_option(
        "", "--lpf", dest="lpf", type="intx", default=True,
        help="Set whether low-pass filtering is used.  [default=%default]")
    return parser


def main(top_block_cls=fshift_pulses, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()

    tb = top_block_cls(center_freq=options.center_freq, f_out=options.f_out, f_in=options.f_in, cutoff=options.cutoff, width=options.width, lpf=options.lpf)
    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()
