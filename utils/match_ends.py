#!/usr/bin/python

"""
Match the phase of two ends of a complex baseband recording from gnuradio.

Finds the starting and ending phase of the streams, takes their complex
conjugate, finds the phases, creates a linear interpolation between them
and multiplies the stream by the normalized value of the corresponding complex
numbers.
"""

import scipy
import numpy as np
import argparse
import logging

logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(logging.WARNING)

def match_ends(stream):
    # Find the starting and ending complex values of the file.
    # Since we want the phases to be 0 in the end, take their conjugates
    beg = stream[:1].mean().conj()
    end = stream[-1:].mean().conj()

    # Find the phases
    beg_phi = np.angle(beg)
    end_phi = np.angle(end)

    # Create the multiplication vector
    mult_phase = np.linspace(beg_phi, end_phi, num=len(stream))
    mult = np.exp(1j * mult_phase)

    # Rotate the source stream accordingly, return it
    return (stream * mult).astype(np.complex64)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=
"""Match the phase of the start and end to zero phase for glitchless looping and
instant lock with a PLL in gnuradio."""
                                     )
    parser.add_argument('files', type=str, nargs='+', help="Files to process.")
    parser.add_argument('-v', dest='verbose', action='count',
                        help="Dump more info. Add 'v'-s for victory.")
    args = parser.parse_args()

    if args.verbose == 1:
        log.setLevel(logging.INFO)
    elif args.verbose == 2:
        log.setLevel(logging.DEBUG)

    log.info('Processing files:')
    for srcfile_name in args.files:
        log.info(srcfile_name)

    for srcfile_name in args.files:
        destname = srcfile_name.rsplit('.', 1)  # split off the extension
        destname = '-matched.'.join(destname)
        with open(srcfile_name) as srcfile:
            stream = scipy.fromfile(srcfile, dtype=scipy.complex64)
            stream = match_ends(stream)
            stream.tofile(destfile)
            log.debug('Processed', srcfile_name)
    print 'Done.'
