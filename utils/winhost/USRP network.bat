rem Make network optimizations for USRP for the duration of the current session (until reboot). DNS servers however need to be reset manually...
rem All options can be made persistent by changing to store=persistent.
rem Needs to be run as administrator.
rem Tested only on Win7 x64.

rem Set IP address and gateway
netsh interface ipv4 set address name="Local Area Connection" static 192.168.10.254 255.255.255.0 none store=active

rem Set DNS servers to none, so our internet, if any, won't try something wonky like accessing webpages through USRP...
netsh interface ipv4 set dnsservers name="Local Area Connection" static none both

rem Increase MTU size to decrease the amount of interrupts that the processor needs to chug through. This enables using 8000-byte frame sizes in UHD
netsh interface ipv4 set subinterface "Local Area Connection" mtu=8192 store=active

pause
