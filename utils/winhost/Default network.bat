rem Reverses the settings made in USRP_network.bat (assuming you use DHCP)
rem Needs to be run as administrator.
rem Tested only on Win7 x64.

netsh interface ipv4 set address name="Local Area Connection" dhcp store=persistent
netsh interface ipv4 set dnsservers name="Local Area Connection" dhcp
netsh interface ipv4 set subinterface "Local Area Connection" mtu=1500 store=persistent

pause
