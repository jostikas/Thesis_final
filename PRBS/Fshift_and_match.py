#!/usr/bin/python
from __future__ import print_function

import numpy as np
import matplotlib.pyplot as pl
import warnings
from re import search
from Thesis.utils import show_plot, fshift_pulses, match_ends

def extract_and_average(src_filename,
                        samp_rate = 25e6/7,
                        rate=None,
                        n_symbols=None,
                        c_freq = -66200,
                        verbose=0
                        ):
    """Extract and average pulses from recorded pulse train files.

    @param src_filename: File name to process
    @param samp_rate: Sample rate. Default is 25 MHz/7
    @param c_freq: Approximate center frequency of the signal. Defaults to -66200 Hz
    @param verbose: 0 - Show no graphs. 1-Show final pulse. 2-Show synced stream. 3-Show all intermediate files.
    @param rate: Symbol rate of the signal. Used for trimming. If None, no trimming is done.
    @param n_symbols: Number of symbols to trim down to. If None, no trimming is done.

    @raise ValueError: If n_symbols is defined, but rate isn't.
    """

    ## Set filenames.
    src = src_filename.rsplit('.', 1)
    fshift_name = '-shifted.'.join(src)
    match_name = '-matched.'.join(src)

    pl.clf()  # Necessary to avoid outputting previous plots on output images.

    ## Plot input data
    if verbose >= 3:
        outdata = np.fromfile(open(src_filename), dtype=np.complex64)
        show_plot(outdata, 'Input data')

    ## Shift the signal frequency, don't low-pass filter. It's a gnuradio flowgraph, so needs starting.
    fshift = fshift_pulses(center_freq=c_freq,
                           f_in=src_filename,
                           f_out=fshift_name,
                           lpf=False
                           )
    fshift.run()

    outdata = np.fromfile(open(fshift_name), dtype=np.complex64)
    ## Plot shifted and filtered signal
    if verbose >= 2:
        show_plot(outdata, 'shifted data')

    ## Trim the file down to n symbols, if necessary values are provided.
    if n_symbols:
        if rate:
            old_length = len(outdata)
            sym_period = samp_rate / rate
            length = int(sym_period * n_symbols)
            outdata = outdata[:length]
            if verbose:
                print('Trimmed data to {} sym ({} smp) from {} sym ({} smp)'.format(
                    n_symbols,
                    len(outdata),
                    old_length/sym_period,
                    old_length
                    ))
        else:
            raise ValueError('Can not trim without knowing symbol rate ("n_symbols" is defined, but "rate" is not.)')

    ## Match the phase of the ends of the signal, normalize.
    outdata = match_ends(outdata)
    outdata /= np.max(np.abs(outdata))

    ## Plot
    if verbose >= 2:
        show_plot(outdata, 'Matched data')

    outdata.tofile(match_name)
    if verbose:
        print('Destination file name:', match_name)
        print('Data type:', outdata.dtype)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="Trim, shift and phase-match the PRBS stream.")

    parser.add_argument('files', type=str, nargs='+', help="Files to process.")
    parser.add_argument('-f','--c_freq', dest='c_freq', type=float, default=-66000.0,
                        help="Approximate center frequency of the signal."
                        )
    parser.add_argument('-s', '--samp_rate', dest='samp_rate', type=float, default=25e6/7,
                        help="Sampling rate of the file. Used for absolute frequency values."
                        )
    parser.add_argument('-v', dest='verbose', action='count',
                        help="Dump more info. Add 'v'-s for victory.")
    parser.add_argument('-l', '--sym_rate', dest='rate', type=float, default=None,
                        help="Sample rate of the file. Used for trimming.")
    parser.add_argument('-n', '--n_symbols', dest='n_symbols', type=int, default=None,
                        help="Number of symbols to trim the file to. sym_rate has to be defined or available in file name.")

    args = parser.parse_args()
    print(args.files)
    for file_name in args.files:

        if args.rate:
            rate = args.rate  # If rate is defined by arguments
        else:  # Otherwise, determine from filename (must contain the '-r100' group)
            # Regexp searches for a float between '-r' and either '-' or '.'
            match = search(r'-r(\d+(\.\d*)?)[-.]', file_name)
            if match:
                rate = float(match.group(1))*1000
                if args.verbose:
                    print("Rate: {} for file {}.".format(rate, file_name))
            else:
                print("Rate couldn't be determined for filename {}".format(file_name))
                continue  # Abort this iteration, continue with next file

        extract_and_average(file_name, args.samp_rate, rate, args.n_symbols, args.c_freq, args.verbose)
        print("Processed {}".format(file_name))
    print("Done.")