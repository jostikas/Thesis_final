#!/usr/bin/python

import numpy as np

class PRBS():
    """Maintain a linear feedback shift register (LFSR), return first bit from it before
    calculating the next one.
    """

    def __init__(self, exponents, seed=None):
        """Create a linear feedback shift register.

        @args exponents: list of feedback exponents. For example (9,5) will create x^9+x^5+1 LFSR.
        @args seed: Initial state of the register. Defaults to binary 1.
        @raise ValueError: If seed value doesn't fit in max(exponents) bits.
        @ret None
        """
        exponents = list(set(exponents))
        bits = max(exponents)
        # Decrement all exponents (they will be used as bitshift values)
        for i in range(len(exponents)):
            exponents[i] -= 1

        # Check the seed for suitability
        if not seed:
            seed = 1
        else:
            seed_len = np.log2(seed)
            if seed_len >= bits:
                raise ValueError('\n'.join(
                                           ('Seed value is longer than the shift register.',
                                           'LFSR length: {} bits.',
                                           'Seed length: {} bits')
                                           ).format(bits, seed_len
                                           )
                )
        self.lfsr = seed
        self.bits = bits
        self.exponents = exponents 

    def __iter__(self):
        return self

    def next(self):
        """Return current first bit, calculate the next one, shift."""
        ret = self.lfsr & 1
        coefficients = [self.lfsr >> e for e in self.exponents]
        newbit = 0
        for c in coefficients:
            newbit ^= c
        newbit &= 1
        self.lfsr = ((self.lfsr << 1) | newbit) & 2**self.bits-1
        return ret

    def tofile(self, filename, bipolar=True, dtype=np.float32):
        """Export a copy of the whole maximum-length sequence starting from current position.

        @args filename: File name to export to.
        @args dtype: Data type to use. Defaults to single-precision float (np.float32)
        """
        if not dtype:
            dtype = np.float16
        length = 2**self.bits - 1
        out = np.fromiter(self, dtype, length)
        if bipolar:
            out *= 2
            out -= 1
        out.tofile(filename)

# Equivalent C implementation used in the devboard
##int PRBS9_output_next_bit(void)
##{
##  static uint16_t reg = 0x0001;  //State of the shift register
##  
##  ret = reg & 1
##  
##  /* Update the state */
##  int newbit = (((reg >> 8) ^ (reg >> 4)) & 0x0001); //x^9+x^5+1, PRBS9
##  reg = ((reg << 1) | newbit) & 0x01FF;
##  return ret
##}

if __name__ == '__main__':
    # Running the file as a script runs tests.
    import os
    import itertools

    testvector  = [1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0]

    print 'Tests:'
    try:
        prbs9 = PRBS((9, 5), 2**9)
    except ValueError:
        print 'Seed length check: PASS'

    prbs9 = PRBS((9, 5), 0x0101)
    a = list(itertools.islice(prbs9, 511))
    if a == testvector:
        print 'Output: PASS'

    testvector = np.fromiter(testvector, dtype=np.int16)
    testvector = testvector.choose(-1, 1)
    prbs9.tofile('TESTFILE.TMP', dtype=np.int16)
    ar = np.fromfile(open('TESTFILE.TMP'), dtype=np.int16)
    if np.array_equal(ar, testvector):
        print 'File export: PASS'
    else:
        print testvector
        print list(ar)
    os.remove('TESTFILE.TMP')


